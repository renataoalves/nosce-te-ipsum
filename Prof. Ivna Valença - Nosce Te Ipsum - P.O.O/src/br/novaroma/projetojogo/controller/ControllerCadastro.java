package br.novaroma.projetojogo.controller;

import java.util.ArrayList;

import javax.swing.JOptionPane;

import br.novaroma.projetojogo.excecoes.ExcecaoJogoJaExiste;
import br.novaroma.projetojogo.excecoes.ExcecaoNomeInvalido;
import br.novaroma.projetojogo.excecoes.ExcecaoNomeNaoInformado;
import br.novaroma.projetojogo.model.Jogo;
import br.novaroma.projetojogo.view.GuiBarra;

public class ControllerCadastro {
	
	private static ArrayList<Jogo> games = new ArrayList<Jogo>();
	private static Jogo gerencia = new Jogo();
	
	private static boolean flag = false;
	
	public static void cadastrarJogo(String nome, String casa, int peoes, String backG, String[] casasEspeciais) throws ExcecaoJogoJaExiste, ExcecaoNomeInvalido, ExcecaoNomeNaoInformado{
		nome = nome.trim();
		verificarNome(nome);
		gerencia.setNomeJogo(nome);
		gerencia.setQntCasas(casa);
		gerencia.setTabuleiro(gerencia.getQntCasas());
		gerencia.setQntPeoes(peoes);
		gerencia.setBackground(backG);
		gerencia.setCasasEspeciais(casasEspeciais);
		
		games.add(new Jogo(gerencia.getNomeJogo(), gerencia.getQntCasas(), gerencia.getTabuleiro(), gerencia.getQntPeoes(), gerencia.getBackground(), gerencia.getCasasEspeciais()));
		
		JOptionPane.showMessageDialog(null, "Jogo cadastrado com sucesso!", "Cadastro de Jogo", 1);
		GuiBarra.ativarItensMenu();
	}
	public static void verificarNome(String name) throws ExcecaoJogoJaExiste, ExcecaoNomeInvalido{
		for (int y=0; y<games.size(); y++){
			flag = (games.get(y).getNomeJogo().equals(name) ? true : false);
			if (flag == true)
				throw new ExcecaoJogoJaExiste();
		}
		
		String[] splitName = name.split("");
		
		for (int y=0; y<splitName.length; y++){
			if(splitName[y].equals(""))
				throw new ExcecaoNomeInvalido();
		}
	}
	
	public static ArrayList<Jogo> getGames() {
		return games;
	}
}