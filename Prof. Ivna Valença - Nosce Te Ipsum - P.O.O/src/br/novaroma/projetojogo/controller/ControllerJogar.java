package br.novaroma.projetojogo.controller;

import java.util.*;

import javax.swing.*;

import br.novaroma.projetojogo.view.GuiAntesDeJogar;
import br.novaroma.projetojogo.view.GuiTabuleiro;
import br.novaroma.projetojogo.view.GuiVencedorDaPartida;

public class ControllerJogar {
	
	private static int indiceJogoEscolhido, casasPular = 0, qntPeoes;
	private static String qntCasas;

	public static int[][] tabuleiro = null;
	public static String[] acaoCasa = null;
	
	private static Random dado = new Random();
	private static int numDado;
	
	public static int x0 = 7, y0 = 0, x1 = 7, y1 = 0, x2 = 7, y2 = 0, x3 = 7, y3 = 0, x4 = 7, y4 = 0;
	
	public static boolean jogarDnv0 = false, jogarDnv1 = false, jogarDnv2 = false, jogarDnv3 = false, jogarDnv4 = false,
								   ficarSemJogar0 = false,  ficarSemJogar1 = false, ficarSemJogar2 = false, ficarSemJogar3 = false, ficarSemJogar4 = false;
	
	public static void faceDado(int numDado){
		if(numDado == 1)
			JOptionPane.showMessageDialog(null, new ImageIcon("face do dado/1.gif"), "Resultado do Dado", -1, null);
		else if(numDado == 2)
			JOptionPane.showMessageDialog(null, new ImageIcon("face do dado/2.gif"), "Resultado do Dado", -1, null);
		else if(numDado == 3)
			JOptionPane.showMessageDialog(null, new ImageIcon("face do dado/3.gif"), "Resultado do Dado", -1, null);
		else if(numDado == 4)
			JOptionPane.showMessageDialog(null, new ImageIcon("face do dado/4.gif"), "Resultado do Dado", -1, null);
		else if(numDado == 5)
			JOptionPane.showMessageDialog(null, new ImageIcon("face do dado/5.gif"), "Resultado do Dado", -1, null);
		else if(numDado == 6)
			JOptionPane.showMessageDialog(null, new ImageIcon("face do dado/6.gif"), "Resultado do Dado", -1, null);
	}
	public static void jogar(int vezDoJogador, String corPeao, String nomePeao){
		
		GuiTabuleiro.i = 60; // para parar o timer
		
		qntPeoes = ControllerCadastro.getGames().get(indiceJogoEscolhido).getQntPeoes();
		qntCasas = ControllerCadastro.getGames().get(indiceJogoEscolhido).getQntCasas();
		
		int linha = 0, coluna = 0;
		
		numDado = dado.nextInt(6)+1;
		faceDado(numDado);
		
		// andar no tabuleiro 
		if(vezDoJogador == 0){
			casasPular = numDado + tabuleiro[x0][y0];
			acaoCasasEspeciais(vezDoJogador);
		}
		if(vezDoJogador == 1){
			casasPular = numDado + tabuleiro[x1][y1];
			acaoCasasEspeciais(vezDoJogador);
		}
		if(vezDoJogador == 2){
			casasPular = numDado + tabuleiro[x2][y2];
			acaoCasasEspeciais(vezDoJogador);
		}
		if(vezDoJogador == 3){
			casasPular = numDado + tabuleiro[x3][y3];
			acaoCasasEspeciais(vezDoJogador);
		}
		if(vezDoJogador == 4){
			casasPular = numDado + tabuleiro[x4][y4];
			acaoCasasEspeciais(vezDoJogador);
		}

		for (int l=0; l <8; l++){
			for (int c=0; c<8; c++){
				if(tabuleiro[l][c] != 0){
					if(casasPular == tabuleiro[l][c]){
						linha = l;
						coluna = c;
						tabuleiro[linha][coluna] = 101+vezDoJogador;
						break;
					} else if(casasPular >= 36 && qntCasas.equals("36 casas")){
						GuiTabuleiro.timer.stop();
						new GuiVencedorDaPartida(corPeao, nomePeao);
						break;
					} else if(casasPular >= 32 && qntCasas.equals("32 casas")){
						GuiTabuleiro.timer.stop();
						new GuiVencedorDaPartida(corPeao, nomePeao);
						break;
					} else if(casasPular >= 28 && qntCasas.equals("28 casas")){
						GuiTabuleiro.timer.stop();
						new GuiVencedorDaPartida(corPeao, nomePeao);
						break;
					}
				}
			}
			if(casasPular >= 36 && qntCasas.equals("36 casas"))
				break;
			else if(casasPular >= 32 && qntCasas.equals("32 casas"))
				break;
			else if(casasPular >= 28 && qntCasas.equals("28 casas"))
				break;
		}
		// andar no tabuleiro *end
		
		if(vezDoJogador == 0){
			x0 = linha;
			y0 = coluna;
		} else if (vezDoJogador == 1){
			x1 = linha;
			y1 = coluna;
		} else if (vezDoJogador == 2){
			x2 = linha;
			y2 = coluna;
		} else if (vezDoJogador == 3){
			x3 = linha;
			y3 = coluna;
		} else if (vezDoJogador == 4){
			x4 = linha;
			y4 = coluna;
		}
		
		/*// impressão da localização
		System.out.println(x0+" Jogador 1 "+y0);
		System.out.println(x1+" Jogador 2 "+y1);
		System.out.println(x2+" Jogador 3 "+y2);
		System.out.println(x3+" Jogador 4 "+y3);
		System.out.println(x4+" Jogador 5 "+y4);
		
		for (int l=0; l<8; l++){
			System.out.print(l+". ");
			for (int c=0; c<8; c++){
				System.out.print(tabuleiro[l][c]);
				System.out.print(" ");
			}
			System.out.println();
		}*/
			
		// muda o valor 101/102/103/104/105 para o valor de antes
		tabuleiro[linha][coluna] = casasPular;
		
		if(qntPeoes == 2){
			if(GuiTabuleiro.a==true)
				GuiTabuleiro.a = false;
			else 
				GuiTabuleiro.a = true;
		}// 2 jogadores
		else if(qntPeoes == 3){
			if(GuiTabuleiro.outro == 0){
				if(GuiTabuleiro.a==true)
					GuiTabuleiro.a = false;
				else {
					GuiTabuleiro.a = true;
					GuiTabuleiro.outro = 1;
				}
			} else {
				if(GuiTabuleiro.a==true)
					GuiTabuleiro.outro = 0;
			}
		}// 3 jogadores
		else if(qntPeoes == 4){
			if(GuiTabuleiro.outro == 0){
				if(GuiTabuleiro.a==true)
					GuiTabuleiro.a = false;
				else {
					GuiTabuleiro.a = true;
					GuiTabuleiro.outro = 1;
				}
			} else {
				if(GuiTabuleiro.a==true)
					GuiTabuleiro.a = false;
				else {
					GuiTabuleiro.a = true;
					GuiTabuleiro.outro = 0;
				}
			}
		}// 4 jogadores
		else { // 5 jogadores
			if(GuiTabuleiro.outro == 0){
				if(GuiTabuleiro.a==true)
					GuiTabuleiro.a = false;
				else {
					GuiTabuleiro.a = true;
					GuiTabuleiro.outro = 1;
				}
			} else if(GuiTabuleiro.outro==1){
				if(GuiTabuleiro.a==true)
					GuiTabuleiro.a = false;
				else {
					GuiTabuleiro.a = true;
					GuiTabuleiro.outro = 2;
				}
			} else if(GuiTabuleiro.outro==2){
				if(GuiTabuleiro.a==true){
					GuiTabuleiro.a = true;
					GuiTabuleiro.outro = 0;
				}
			}
		}// 5 jogadores
		
		if(casasPular >= 36 && qntCasas.equals("36 casas"))
			zerarComponentes(qntCasas);
		else if(casasPular >= 32 && qntCasas.equals("32 casas"))
			zerarComponentes(qntCasas);
		else if(casasPular >= 28 && qntCasas.equals("28 casas"))
			zerarComponentes( qntCasas);
	}
	private static void acaoCasasEspeciais(int vezDoJogador){
		if(casasPular == Integer.parseInt(acaoCasa[0]) || casasPular == Integer.parseInt(acaoCasa[2]) || casasPular == Integer.parseInt(acaoCasa[4]) || casasPular == Integer.parseInt(acaoCasa[6]) || casasPular == Integer.parseInt(acaoCasa[8])){
			if(casasPular == Integer.parseInt(acaoCasa[0])){
				if(acaoCasa[1].equals("Pular uma casa")){
					JOptionPane.showMessageDialog(null, null, "Pule uma casa", -1, new ImageIcon("casas especiais/pule.png"));
					casasPular+=2;
				}
				if(acaoCasa[1].equals("Ficar sem jogar uma rodada")){
					JOptionPane.showMessageDialog(null, null, "Fique sem jogar uma rodade", -1, new ImageIcon("casas especiais/stop.png"));
					if(vezDoJogador == 0)
						ficarSemJogar0 = true;
					if(vezDoJogador == 1)
						ficarSemJogar1 = true;
					if(vezDoJogador == 2)
						ficarSemJogar2 = true;
					if(vezDoJogador == 3)
						ficarSemJogar3 = true;
					if(vezDoJogador == 4)
						ficarSemJogar4 = true;
				}
				if(acaoCasa[1].equals("Jogue outra vez")){
					JOptionPane.showMessageDialog(null, null, "Jogue outra vez", -1, new ImageIcon("casas especiais/jogue.png"));
					if(vezDoJogador == 0)
						jogarDnv0 = true;
					if(vezDoJogador == 1)
						jogarDnv1 = true;
					if(vezDoJogador == 2)
						jogarDnv2 = true;
					if(vezDoJogador == 3)
						jogarDnv3 = true;
					if(vezDoJogador == 4)
						jogarDnv4 = true;
				}
			} else if(casasPular == Integer.parseInt(acaoCasa[2])){
				if(acaoCasa[3].equals("Pular uma casa")){
					JOptionPane.showMessageDialog(null, null, "Pule uma casa", -1, new ImageIcon("casas especiais/pule.png"));
					casasPular+=2;
				}
				if(acaoCasa[3].equals("Ficar sem jogar uma rodada")){
					JOptionPane.showMessageDialog(null, null, "Fique sem jogar uma rodade", -1, new ImageIcon("casas especiais/stop.png"));
					if(vezDoJogador == 0)
						ficarSemJogar0 = true;
					if(vezDoJogador == 1)
						ficarSemJogar1 = true;
					if(vezDoJogador == 2)
						ficarSemJogar2 = true;
					if(vezDoJogador == 3)
						ficarSemJogar3 = true;
					if(vezDoJogador == 4)
						ficarSemJogar4 = true;
				}
				if(acaoCasa[3].equals("Jogue outra vez")){
					JOptionPane.showMessageDialog(null, null, "Jogue outra vez", -1, new ImageIcon("casas especiais/jogue.png"));
					if(vezDoJogador == 0)
						jogarDnv0 = true;
					if(vezDoJogador == 1)
						jogarDnv1 = true;
					if(vezDoJogador == 2)
						jogarDnv2 = true;
					if(vezDoJogador == 3)
						jogarDnv3 = true;
					if(vezDoJogador == 4)
						jogarDnv4 = true;
				}
			} else if(casasPular == Integer.parseInt(acaoCasa[4])){
				if(acaoCasa[5].equals("Pular uma casa")){
					JOptionPane.showMessageDialog(null, null, "Pule uma casa", -1, new ImageIcon("casas especiais/pule.png"));
					casasPular+=2;
				}
				if(acaoCasa[5].equals("Ficar sem jogar uma rodada")){
					JOptionPane.showMessageDialog(null, null, "Fique sem jogar uma rodade", -1, new ImageIcon("casas especiais/stop.png"));
					if(vezDoJogador == 0)
						ficarSemJogar0 = true;
					if(vezDoJogador == 1)
						ficarSemJogar1 = true;
					if(vezDoJogador == 2)
						ficarSemJogar2 = true;
					if(vezDoJogador == 3)
						ficarSemJogar3 = true;
					if(vezDoJogador == 4)
						ficarSemJogar4 = true;
				}
				if(acaoCasa[5].equals("Jogue outra vez")){
					JOptionPane.showMessageDialog(null, null, "Jogue outra vez", -1, new ImageIcon("casas especiais/jogue.png"));
					if(vezDoJogador == 0)
						jogarDnv0 = true;
					if(vezDoJogador == 1)
						jogarDnv1 = true;
					if(vezDoJogador == 2)
						jogarDnv2 = true;
					if(vezDoJogador == 3)
						jogarDnv3 = true;
					if(vezDoJogador == 4)
						jogarDnv4 = true;
				}
			} else if(casasPular == Integer.parseInt(acaoCasa[6])){
				if(acaoCasa[7].equals("Pular uma casa")){
					JOptionPane.showMessageDialog(null, null, "Pule uma casa", -1, new ImageIcon("casas especiais/pule.png"));
					casasPular+=2;
				}
				if(acaoCasa[7].equals("Ficar sem jogar uma rodada")){
					JOptionPane.showMessageDialog(null, null, "Fique sem jogar uma rodade", -1, new ImageIcon("casas especiais/stop.png"));
					if(vezDoJogador == 0)
						ficarSemJogar0 = true;
					if(vezDoJogador == 1)
						ficarSemJogar1 = true;
					if(vezDoJogador == 2)
						ficarSemJogar2 = true;
					if(vezDoJogador == 3)
						ficarSemJogar3 = true;
					if(vezDoJogador == 4)
						ficarSemJogar4 = true;
				}
				if(acaoCasa[7].equals("Jogue outra vez")){
					JOptionPane.showMessageDialog(null, null, "Jogue outra vez", -1, new ImageIcon("casas especiais/jogue.png"));
					if(vezDoJogador == 0)
						jogarDnv0 = true;
					if(vezDoJogador == 1)
						jogarDnv1 = true;
					if(vezDoJogador == 2)
						jogarDnv2 = true;
					if(vezDoJogador == 3)
						jogarDnv3 = true;
					if(vezDoJogador == 4)
						jogarDnv4 = true;
				}
			} else if(casasPular == Integer.parseInt(acaoCasa[8])){
				if(acaoCasa[9].equals("Pular uma casa")){
					JOptionPane.showMessageDialog(null, null, "Pule uma casa", -1, new ImageIcon("casas especiais/pule.png"));
					casasPular+=2;
				}
				if(acaoCasa[9].equals("Ficar sem jogar uma rodada")){
					JOptionPane.showMessageDialog(null, null, "Fique sem jogar uma rodade", -1, new ImageIcon("casas especiais/stop.png"));
					if(vezDoJogador == 0)
						ficarSemJogar0 = true;
					if(vezDoJogador == 1)
						ficarSemJogar1 = true;
					if(vezDoJogador == 2)
						ficarSemJogar2 = true;
					if(vezDoJogador == 3)
						ficarSemJogar3 = true;
					if(vezDoJogador == 4)
						ficarSemJogar4 = true;
				}
				if(acaoCasa[9].equals("Jogue outra vez")){
					JOptionPane.showMessageDialog(null, null, "Jogue outra vez", -1, new ImageIcon("casas especiais/jogue.png"));
					if(vezDoJogador == 0)
						jogarDnv0 = true;
					if(vezDoJogador == 1)
						jogarDnv1 = true;
					if(vezDoJogador == 2)
						jogarDnv2 = true;
					if(vezDoJogador == 3)
						jogarDnv3 = true;
					if(vezDoJogador == 4)
						jogarDnv4 = true;
				}
			}
		}
	}
	private static void zerarComponentes(String numCasas){
		casasPular = 0;
		tabuleiro = null;
		acaoCasa = null;
		
		x0 = 7;
		y0 = 0;
		x1 = 7;
		y1 = 0;
		x2 = 7;
		y2 = 0;
		x3 = 7;
		y3 = 0;
		x4 = 7;
		y4 = 0;
		
 		jogarDnv0 = false;
		jogarDnv1 = false;
		jogarDnv2 = false;
		jogarDnv3 = false;
		jogarDnv4 = false;
		
		ficarSemJogar0 = false;
		ficarSemJogar1 = false;
		ficarSemJogar2 = false;
		ficarSemJogar3 = false;
		ficarSemJogar4 = false;
		GuiAntesDeJogar.gui.setVisible(false);
	}
	/*private static void atualizarTabuleiro(String numCasas) {
		
		int numDeCasas = 0;
		
		if(numCasas.equals("36 casas"))
			numDeCasas = 35;
		else if(numCasas.equals("32 casas"))
			numDeCasas = 31;
		else if(numCasas.equals("28 casas"))
			numDeCasas = 27;
		
		int [][] tabuleiroX = new int [8][8];
		int naoExiste = 0;
		
		if(numDeCasas == 35){
			for (int x = 0; x<8; x++){
				for (int y = 0; y<8; y++){
				
					if(y==0)
						tabuleiroX[x][y] = 7-x;
					else if (y==2)
						tabuleiroX[x][y] = 9+x;
					else if (y==4)
						tabuleiroX[x][y] = 25-x;
					else if (y==6)
						tabuleiroX[x][y] = 27+x;
					
					if(x==0){
						if(y==3 || y==7)
							tabuleiroX[x][y] = naoExiste;
						else if (y==1)
							tabuleiroX[x][y] = 8;
						else if (y==5)
							tabuleiroX[x][y] = 26;
					} else if(x==1){
						if (y==1 || y==3 || y==5 || y==7)
							tabuleiroX [x][y] = naoExiste;
					} else if(x==2){
						if (y==1 || y==3 || y==5 || y==7)
							tabuleiroX [x][y] = naoExiste;
					} else if(x==3){
						if (y==1 || y==3 || y==5 || y==7)
							tabuleiroX [x][y] = naoExiste;
					} else if(x==4){
						if (y==1 || y==3 || y==5 || y==7)
							tabuleiroX [x][y] = naoExiste;
					} else if(x==5){
						if (y==1 || y==3 || y==5 || y==7)
							tabuleiroX [x][y] = naoExiste;
					} else if(x==6){
						if (y==1 || y==3 || y==5 || y==7)
							tabuleiroX [x][y] = naoExiste;
					} else if(x==7){
						if (y==1 || y==5)
							tabuleiroX [x][y] = naoExiste;
						else if(y==3)
							tabuleiroX [x][y] = 17;
						else if (y==7)
							tabuleiroX[x][y] = 35;
					}
				}
			}
			tabuleiro = tabuleiroX;
		} else if(numDeCasas == 31){
			for (int x = 0; x<8; x++){
				for (int y = 0; y<8; y++){
					
					if(y==0)
						tabuleiroX[x][y] = 7-x;
					else if (y==2)
						tabuleiroX[x][y] = 7+x;
					else if (y==4)
						tabuleiroX[x][y] = 23-x;
					else if (y==6)
						tabuleiroX[x][y] = 23+x;
					
					if(x==0)
						tabuleiroX[x][y] = naoExiste;
					else if(x==1){						
						if (y==3 || y==7)
							tabuleiroX [x][y] = naoExiste;
						else if (y==1)
							tabuleiroX[x][y] = 7;
						else if (y==5)
							tabuleiroX[x][y] = 23;
					} else if(x==2){
						if (y==1 || y==3 || y==5 || y==7)
							tabuleiroX [x][y] = naoExiste;
					} else if(x==3){
						if (y==1 || y==3 || y==5 || y==7)
							tabuleiroX [x][y] = naoExiste;
					} else if(x==4){
						if (y==1 || y==3 || y==5 || y==7)
							tabuleiroX [x][y] = naoExiste;
					} else if(x==5){
						if (y==1 || y==3 || y==5 || y==7)
							tabuleiroX [x][y] = naoExiste;
					} else if(x==6){
						if (y==1 || y==3 || y==5 || y==7)
							tabuleiroX [x][y] = naoExiste;
					} else if(x==7){
						if (y==1 || y==5)
							tabuleiroX [x][y] = naoExiste;
						else if (y==3)
							tabuleiroX[x][y] = 15;
						else if (y==7)
							tabuleiroX[x][y] = 31;
					}
				} 
			}
			tabuleiro = tabuleiroX;
		} else if(numDeCasas == 27){
			for (int x = 0; x<8; x++){
				for (int y = 0; y<8; y++){
					
					if(y==0)
						tabuleiroX[x][y] = 6-x;
					else if (y==2)
						tabuleiroX[x][y] = 6+x;
					else if (y==4)
						tabuleiroX[x][y] = 20-x;
					else if (y==6)
						tabuleiroX[x][y] = 20+x;
					
					if(x==0)
						tabuleiroX[x][y] = naoExiste;
					else if(x==1){
						if (y==3 || y==7)
							tabuleiroX [x][y] = naoExiste;
						else if(y==1)
							tabuleiroX[x][y] = 6;
						else if(y==5)
							tabuleiroX[x][y] = 20;
					} else if(x==2){
						if (y==1 || y==3 || y==5 || y==7)
							tabuleiroX[x][y] = naoExiste;
					} else if(x==3){
						if (y==1 || y==3 || y==5 || y==7)
							tabuleiroX[x][y] = naoExiste;
					} else if(x==4){
						if (y==1 || y==3 || y==5 || y==7)
							tabuleiroX[x][y] = naoExiste;
					} else if(x==5){
						if (y==1 || y==3 || y==5 || y==7)
							tabuleiroX[x][y] = naoExiste;
					} else if(x==6){
						if (y==1 || y==5)
							tabuleiroX[x][y] = naoExiste;
						else if(y==3)
							tabuleiroX[x][y] = 13;
						else if(y==7)
							tabuleiroX[x][y] = 27;
					} else if(x==7)
						tabuleiroX[x][y] = naoExiste;
				} 
			}
			tabuleiro = tabuleiroX;
		}
		
	}
	*/
	public static int getIndiceJogoEscolhido(){
		return indiceJogoEscolhido;
	}
	public static void setIndiceJogoEscolhido(int indexJogoEscolhido){
		indiceJogoEscolhido = indexJogoEscolhido;
	}
}