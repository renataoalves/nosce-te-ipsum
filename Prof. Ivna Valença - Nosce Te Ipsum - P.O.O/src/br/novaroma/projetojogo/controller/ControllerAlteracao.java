package br.novaroma.projetojogo.controller;

import javax.swing.JOptionPane;

import br.novaroma.projetojogo.excecoes.ExcecaoJogoJaExiste;
import br.novaroma.projetojogo.excecoes.ExcecaoNomeInvalido;
import br.novaroma.projetojogo.excecoes.ExcecaoNomeNaoInformado;
import br.novaroma.projetojogo.view.GuiAlteracao;
import br.novaroma.projetojogo.view.GuiAntesDeJogar;
import br.novaroma.projetojogo.view.GuiBarra;
import br.novaroma.projetojogo.view.GuiCadastrar;
import br.novaroma.projetojogo.view.GuiInformacaoJogo;

public class ControllerAlteracao{
	
	private static String nome;
	private static int opc;
	private static Object[] opcao = {"Sim", "N�o"}, jogo;
	private static boolean flag = false;
	
	public static void alteracao(){
		nomesDosJogos();
		nome = (String) JOptionPane.showInputDialog(null, "De qual jogo deseja fazer a altera��o de nome?", "Altera��o de Nome de Jogo", -1, null, jogo, jogo[0]);

		if(nome != null){
			buscar();
			new GuiAlteracao(ControllerCadastro.getGames().get(ControllerJogar.getIndiceJogoEscolhido()).getNomeJogo(), 
								 ControllerCadastro.getGames().get(ControllerJogar.getIndiceJogoEscolhido()).getQntCasas(),
								 ControllerCadastro.getGames().get(ControllerJogar.getIndiceJogoEscolhido()).getQntPeoes(),
								 ControllerCadastro.getGames().get(ControllerJogar.getIndiceJogoEscolhido()).getBackground());
		}
	}
	public static void alterarNome(String renomear){
		try {
			renomear = renomear.trim();
			ControllerCadastro.verificarNome(renomear);
			ControllerCadastro.getGames().get(ControllerJogar.getIndiceJogoEscolhido()).setNomeJogo(renomear);
			JOptionPane.showMessageDialog(null, "Altera��es realizadas com sucesso!", "Altera��o de Componentes do Jogo", 1);
		} catch (ExcecaoNomeNaoInformado e) {
			JOptionPane.showMessageDialog(null, "Nome de jogo n�o alterado. \n\nJogo n�o cadastrado!", "Cadastro N�o Realizado", 2);
		} catch (ExcecaoJogoJaExiste e) {
		} catch (ExcecaoNomeInvalido e) {
		}
	}
 	public static void buscarJogo(){
		try{
			nome = JOptionPane.showInputDialog(null, "(OBS: Quando digitar, digite exatamente como foi cadastrado).\nQual o nome do jogo que deseja buscar?", "Buscar Jogo", -1);
			if(!(nome.equals("")) && nome != null){
				if(buscar() == true)
					new GuiInformacaoJogo(ControllerCadastro.getGames().get(ControllerJogar.getIndiceJogoEscolhido()).getNomeJogo(),
													 ControllerCadastro.getGames().get(ControllerJogar.getIndiceJogoEscolhido()).getQntCasas(),
													 ControllerCadastro.getGames().get(ControllerJogar.getIndiceJogoEscolhido()).getQntPeoes(),
													 ControllerCadastro.getGames().get(ControllerJogar.getIndiceJogoEscolhido()).getBackground(),
													 ControllerCadastro.getGames().get(ControllerJogar.getIndiceJogoEscolhido()).getCasasEspeciais());
				else {
					opc = JOptionPane.showOptionDialog(null, "N�o existe um jogo cadastrado com esse nome. \nDeseja tentar novamente?", "Buscar N�o Efetuada", 2, 0, null, opcao, opcao[0]);
					if (opc == 0)
						buscarJogo();
				}
			} else 
				JOptionPane.showMessageDialog(null, "Nenhum nome de jogo foi digitado para fazer a busca", "Buscar Jogo", 2);
		} catch (NullPointerException e){
		}
	}
	public static void excluirJogo(){
		try{
			nomesDosJogos();
			nome = (String) JOptionPane.showInputDialog(null, "Qual jogo deseja excluir?", "Excluir Jogo", -1, null, jogo, jogo[0]);
			if(buscar() == true){
				opc = JOptionPane.showOptionDialog(null, "Tem certeza que deseja excluir jogo?", "Excluir Jogo", -1, 0, null, opcao, opcao[0]);
				if(opc == 0){
					ControllerCadastro.getGames().remove(ControllerCadastro.getGames().get(ControllerJogar.getIndiceJogoEscolhido()));
					JOptionPane.showMessageDialog(null, "Jogo exclu�do com sucesso", "Excluir Jogo", 1);
					GuiBarra.ativarItensMenu();
				}
			}
		} catch (NullPointerException e){
		}
	}
	private static void nomesDosJogos(){
		jogo = new Object[ControllerCadastro.getGames().size()];
		
		for (int x=0; x<ControllerCadastro.getGames().size(); x++)
			jogo[x] = ControllerCadastro.getGames().get(x).getNomeJogo();
	}
	private static boolean buscar(){
		for (int y=0; y<ControllerCadastro.getGames().size(); y++){
			flag = (ControllerCadastro.getGames().get(y).getNomeJogo()).equals(nome) ? true : false;
			if (flag == true){
				ControllerJogar.setIndiceJogoEscolhido(y);
				flag = true;
				break;
			} else 
				flag = false;
		}
		return flag;
	}
	public static void escolherJogo(){
		try{
			if(ControllerCadastro.getGames().size() > 0){
				jogo = new Object[ControllerCadastro.getGames().size()];
		
				for (int x=0; x<ControllerCadastro.getGames().size(); x++)
					jogo[x] = ControllerCadastro.getGames().get(x).getNomeJogo();
		
				nome = (String) JOptionPane.showInputDialog(null, "Escolha um dos jogos", "Escolher Jogo", -1, null, jogo, jogo[0]);
				
				if(buscar() == true){
					ControllerJogar.tabuleiro = ControllerCadastro.getGames().get(ControllerJogar.getIndiceJogoEscolhido()).getTabuleiro();
					ControllerJogar.acaoCasa = ControllerCadastro.getGames().get(ControllerJogar.getIndiceJogoEscolhido()).getCasasEspeciais();
					new GuiAntesDeJogar(ControllerCadastro.getGames().get(ControllerJogar.getIndiceJogoEscolhido()).getQntPeoes(),
							ControllerCadastro.getGames().get(ControllerJogar.getIndiceJogoEscolhido()).getQntCasas(),
							ControllerCadastro.getGames().get(ControllerJogar.getIndiceJogoEscolhido()).getBackground());
				}
			
			} else {
				opc = JOptionPane.showOptionDialog(null, "N�o h� nenhum jogo cadastrado. \nDeseja cadastrar um novo jogo?", "Nenhum Jogo Cadastrado", 2, 2, null, opcao, opcao[0]);
				if (opc == 0)
					new GuiCadastrar();
			}
		} catch (NullPointerException e){
			JOptionPane.showMessageDialog(null, "Jogo n�o escolhido ! [NullPointerException]");
		} catch (NumberFormatException e){
			JOptionPane.showMessageDialog(null, "Jogo n�o escolhido ! [NumberFormatException]");
		}
	}
}