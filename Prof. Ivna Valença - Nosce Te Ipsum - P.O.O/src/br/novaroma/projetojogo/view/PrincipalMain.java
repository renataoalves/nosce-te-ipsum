package br.novaroma.projetojogo.view;

import java.awt.EventQueue;

public class PrincipalMain {
	
	public static GuiMenu menu;
	
	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				menu = new GuiMenu();
			}
		});
		
	}
}