package br.novaroma.projetojogo.view;

import java.awt.*;

import javax.swing.*;

public class GuiVencedorDaPartida extends JFrame{
	private static final long serialVersionUID = 1L;
	
	private JLabel vencedor,
						gif1 = new JLabel(new ImageIcon("wingifs/ganhou2.gif")),
						gif2 = new JLabel(new ImageIcon("wingifs/ganhou1.gif"));
	private JPanel panelVencedor = new JPanel(new GridBagLayout()), wins;
	private GridBagConstraints c = new GridBagConstraints();
	
	public GuiVencedorDaPartida(String corVencedor, String nomeVencedor){
		super("Vencedor Da Partida");
		
		setIconImage((new ImageIcon(getClass().getResource("/imagens/icone.png"))).getImage());
		
		wins = new Background(winner(corVencedor));
		wins.setLayout(new GridBagLayout());
		
		vencedor = new JLabel(nomeVencedor);
		
		vencedor.setFont(new Font("Verdana", Font.PLAIN, 35));
		panelVencedor.add(vencedor);
		panelVencedor.setBackground(Color.white);
		
		c.gridx = 0;
		c.gridy = 0;
		panelVencedor.add(gif1, c);
		c.gridy = 1;
		panelVencedor.add(vencedor, c);
		c.gridy = 2;
		panelVencedor.add(gif2, c);
		
		panelVencedor.setBackground(Color.WHITE);
		
		wins.add(panelVencedor);
		
		add(wins);  
		
		GuiTabuleiro.zerarPosicaoPeao();
		GuiTabuleiro.i = 60;
		GuiTabuleiro.timer.stop();
		GuiTabuleiro.a = true;
		GuiTabuleiro.outro = 0;
		PrincipalMain.menu.setVisible(true);
		
		setVisible(true);
		setResizable(false);
		setSize(1000, 530);
		setLocationRelativeTo(null);
	}
	
	private String winner(String corVencedor){
		if(corVencedor.equals("Verde"))
			return "/wins/winsVerde.png";
		else if(corVencedor.equals("Amarelo"))
			return "/wins/winsAmarelo.png";
		else if(corVencedor.equals("Azul"))
			return "/wins/winsAzul.png";
		else if(corVencedor.equals("Branco"))
			return "/wins/winsBranco.png";
		else
			return "/wins/winsRoxo.png";
	}
}
