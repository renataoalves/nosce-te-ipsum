package br.novaroma.projetojogo.view;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class Background extends JPanel {  
	private static final long serialVersionUID = 1L;
	
	private BufferedImage b;  
    private Rectangle2D rect;  
  
    public Background(String image) {  
        try {  
            b = ImageIO.read(Thread.currentThread().getClass().getResourceAsStream(image));//carrega a imagem  
            rect = new Rectangle(0, 0, b.getWidth(), b.getHeight());//cria um retangulo com o tamanho da imagem  
  
        } catch (IOException ex) {  
            ex.printStackTrace(System.err);  
        }  
    }  
  
    @Override  
    public void paintComponent(Graphics g) {   
        TexturePaint p = new TexturePaint(b, rect);//preenche a tela com esses retangulos  
        Graphics2D g2 = (Graphics2D) g;  
        g2.setPaint(p);//diz pro grafico da tela que vai fazer um texturePaint  
        g2.fillRect(0, 0, this.getWidth(), this.getHeight());//desenha a textura na tela toda, preenchendo com a imagem escolhida  
  
    }  
}