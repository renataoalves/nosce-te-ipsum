package br.novaroma.projetojogo.view;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import br.novaroma.projetojogo.controller.ControllerAlteracao;
import br.novaroma.projetojogo.controller.ControllerCadastro;
import br.novaroma.projetojogo.controller.ControllerJogar;

public class GuiBarra extends JFrame {
	private static final long serialVersionUID = 1L; 

	private JMenu menuJogo = new JMenu("Jogo"),
			 			 menuSobre = new JMenu("Sobre"),
			 			 menuSair = new JMenu("Sair");
	private static JMenuItem novoJogo, alterar, buscar, excluir, jogo, desenvolvedores, sair;
	private AcaoDosItens tratador = new AcaoDosItens();
	private JMenuBar menuBar = new JMenuBar();
	static ControllerJogar control;
	
	public GuiBarra(){
		super("Nosce Te Ipsum");
		setLayout(new FlowLayout());
		
		novoJogo = new JMenuItem("Novo Jogo", new ImageIcon(getClass().getResource("/icones/plus.png")));
		alterar = new JMenuItem("Alterar", new ImageIcon(getClass().getResource("/icones/peao.png")));
		buscar = new JMenuItem("Buscar", new ImageIcon(getClass().getResource("/icones/lupa.png")));
		excluir = new JMenuItem("Excluir", new ImageIcon(getClass().getResource("/icones/x.png")));
		
		jogo = new JMenuItem("Jogo", new ImageIcon(getClass().getResource("/icones/manual.png")));
		desenvolvedores = new JMenuItem("Desenvolvedores", new ImageIcon(getClass().getResource("/icones/java.png")));
		
		sair = new JMenuItem("Sair", new ImageIcon(getClass().getResource("/icones/exit.png")));
		
		ativarItensMenu();
		
		setIconImage((new ImageIcon(getClass().getResource("/imagens/icone.png"))).getImage());
		
		menuJogo.setToolTipText("Alt + 1");
		menuJogo.setMnemonic(KeyEvent.VK_1);
		novoJogo.setAccelerator(KeyStroke.getKeyStroke("control N"));
		alterar.setAccelerator(KeyStroke.getKeyStroke("control A"));
    	buscar.setAccelerator(KeyStroke.getKeyStroke("control B"));
    	excluir.setAccelerator(KeyStroke.getKeyStroke("control E"));
    	menuJogo.add(novoJogo);
    	menuJogo.add(alterar);
		menuJogo.add(buscar);
		menuJogo.add(excluir);
		
		menuSobre.setToolTipText("Alt + 2");
		menuSobre.setMnemonic(KeyEvent.VK_2);
		jogo.setAccelerator(KeyStroke.getKeyStroke("control H"));
		desenvolvedores.setAccelerator(KeyStroke.getKeyStroke("control D"));
		menuSobre.add(jogo);
		menuSobre.add(desenvolvedores);
		
		menuSair.setToolTipText("Alt + 3");
		menuSair.setMnemonic(KeyEvent.VK_3);
		sair.setAccelerator(KeyStroke.getKeyStroke("control S"));
		menuSair.add(sair);
		
		menuBar.add(menuJogo);
		menuBar.add(menuSobre);
		menuBar.add(menuSair);
		setJMenuBar(menuBar);

		novoJogo.addActionListener(tratador);
		alterar.addActionListener(tratador);
		buscar.addActionListener(tratador);
		excluir.addActionListener(tratador);
		
		sair.addActionListener(tratador);
		
		jogo.addActionListener(tratador);
		desenvolvedores.addActionListener(tratador);
		
		setVisible(true);
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public static void ativarItensMenu(){
		if(ControllerCadastro.getGames().size() == 0){
			buscar.setEnabled(false);
			excluir.setEnabled(false);
			alterar.setEnabled(false);
		} else {
			buscar.setEnabled(true);
			excluir.setEnabled(true);
			alterar.setEnabled(true);
		}
	}
	private class AcaoDosItens implements ActionListener{
		public void actionPerformed(ActionEvent e){
			if(e.getSource() == novoJogo)
				new GuiCadastrar();
			else if(e.getSource() == alterar)
				ControllerAlteracao.alteracao();
			else if(e.getSource() == buscar)
				ControllerAlteracao.buscarJogo();
			else if(e.getSource() == excluir)
				ControllerAlteracao.excluirJogo();
			
			else if (e.getSource() == jogo)
				new GuiManualJogo();
			
			else if (e.getSource() == desenvolvedores)
				JOptionPane.showMessageDialog(null, "Desenvolvido por Paulo Renato e Zuamir Gutembergue", "Desenvolvedores", -1, new ImageIcon(getClass().getResource("/imagens/brasil.png")));

			else if (e.getSource() == sair)
				System.exit(0);
		}
	}
}