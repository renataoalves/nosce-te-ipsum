package br.novaroma.projetojogo.view;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

import javax.swing.*;

public class GuiInformacaoJogo extends JFrame{
	private static final long serialVersionUID = 1L;

	private JLabel nomeJogo, quantCasas, quantPeoes, casasEspe, background, casa1, casa2, casa3, casa4, casa5; 
	private JPanel panelJLabels = new JPanel(new GridBagLayout()),
			 			panelEspecial = new JPanel(new GridBagLayout()),
			 			painelPeao = new JPanel(new GridBagLayout()),
			 			panelTabuleiro = new JPanel(new GridBagLayout()),
			 			painelBackg = new JPanel(),
			 			panelButton = new JPanel(new FlowLayout());
	private JButton back = new JButton("Voltar");
	private GridBagConstraints l = new GridBagConstraints(), c = new GridBagConstraints(), backg = new GridBagConstraints();
	
	public GuiInformacaoJogo(String nome, String qntCasas, int qntPeoes, String backG, String[] casasEspeciais){
		super("Informa��es do Jogo");
		setLayout(new GridBagLayout());
		
		setIconImage((new ImageIcon(getClass().getResource("/imagens/icone.png"))).getImage());
		
		nomeJogo = new JLabel("Nome do jogo: "+nome);
		quantCasas = new JLabel("Quantidade de Casas: "+qntCasas);
		quantPeoes = new JLabel ("Quantidade de Pe�es: "+qntPeoes+" pe�es");
		 casasEspe = new JLabel ("Casas especiais: ");
		
		nomeJogo.setFont(new Font("Verdana", Font.PLAIN, 17));
		quantCasas.setFont(new Font("Verdana", Font.PLAIN, 17));
		quantPeoes.setFont(new Font("Verdana", Font.PLAIN, 17));
		casasEspe.setFont(new Font("Verdana", Font.PLAIN, 17));
		
		l.insets = new Insets(5,0,0,0);
		l.gridx = 0;
		l.gridy = 0;
		panelJLabels.add(nomeJogo, l);
		l.gridy = 1;
		panelJLabels.add(quantCasas, l);
		l.gridy = 2;
		panelJLabels.add(quantPeoes, l);
		l.gridy = 3;
		panelJLabels.add(casasEspe, l);
		
		l.anchor = GridBagConstraints.WEST;
		
		l.gridy = 1;
		casa1 = new JLabel("Casa: "+casasEspeciais[0]+" - A��o: "+casasEspeciais[1]);
		casa1.setFont(new Font("Verdana", Font.PLAIN, 15));
		panelEspecial.add(casa1, l);
		
		l.gridy = 2;
		casa2 = new JLabel("Casa: "+casasEspeciais[2]+" - A��o: "+casasEspeciais[3]);
		casa2.setFont(new Font("Verdana", Font.PLAIN, 15));
		panelEspecial.add(casa2, l);
		
		l.gridy = 3;
		casa3 = new JLabel("Casa: "+casasEspeciais[4]+" - A��o: "+casasEspeciais[5]);
		casa3.setFont(new Font("Verdana", Font.PLAIN, 15));
		panelEspecial.add(casa3, l);
		
		l.gridy = 4;
		casa4 = new JLabel("Casa: "+casasEspeciais[6]+" - A��o: "+casasEspeciais[7]);
		casa4.setFont(new Font("Verdana", Font.PLAIN, 15));
		panelEspecial.add(casa4, l);
		
		l.gridy = 5;
		casa5 = new JLabel("Casa: "+casasEspeciais[8]+" - A��o: "+casasEspeciais[9]);
		casa5.setFont(new Font("Verdana", Font.PLAIN, 15));
		panelEspecial.add(casa5, l);
		
		l.anchor = GridBagConstraints.CENTER;
		l.gridy = 4;
		panelJLabels.add(panelEspecial, l);
		
		c.insets = new Insets(0,0,10,0);
		c.gridx = 0;
		c.gridy = 3;
		panelJLabels.add(painelPeao, c);
		
		if(backG.equals("Azul Claro"))
				background = new JLabel(new ImageIcon(redimensionar(200, 200, new ImageIcon(getClass().getResource("/imagens/azul.png")).getImage())));
		else
				background = new JLabel(new ImageIcon(redimensionar(200, 200, new ImageIcon(backG).getImage()))); 
		
		painelBackg.add(background);
		JLabel labelBackg = new JLabel("Plano de fundo do tabuleiro");
		labelBackg.setFont(new Font("Verdana", Font.PLAIN, 17));
		
		backg.insets = new Insets(0,5,0,0);
		backg.gridx = 1;
		backg.gridy = 0;
		panelTabuleiro.add(labelBackg, backg);
		backg.gridx = 1;
		backg.gridy = 1;
		panelTabuleiro.	add(painelBackg, backg);

		back.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				dispose();
			}
		});
		panelButton.add(back);
		
		c.gridx = 0;
		c.gridy = 0;
		add(panelJLabels, c);
		
		c.gridx = 1;
		c.gridy = 0;
		add(panelTabuleiro, c);
		
		c.anchor = GridBagConstraints.EAST;
		c.gridx = 0;
		c.gridy = 1;
		add(panelButton, c);
		
		pack();
		setVisible(true);
		setResizable(false);
		setLocationRelativeTo(null);
	}
	
	private Image redimensionar(int x, int y, Image imagem){
	       
		BufferedImage render = new BufferedImage (x, y, BufferedImage.TYPE_INT_RGB); // rederizador

	    Graphics2D g2 = render.createGraphics (); //criar objeto em 2d
	    g2.setRenderingHint (RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
	    g2.drawImage(imagem, 0, 0, x, y, null); //cria imagem dimensionada
	    g2.dispose (); // limpa a imagem anterior para liberar outra
	    
	    return render;
	 }
}
