package br.novaroma.projetojogo.view;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import br.novaroma.projetojogo.excecoes.ExcecaoCorPeaoJaEscolhida;
import br.novaroma.projetojogo.excecoes.ExcecaoNomePeaoJaEscolhido;

public class GuiAntesDeJogar extends JFrame implements ActionListener{
	
/////////////////////////// ExcecaoNomePeaoJaEscolhido() N�O T� PEGANDO. Est� no m�todo vetorNome()
	
	private static final long serialVersionUID = 1L;
	
	private JPanel panelOutros = new JPanel(new GridBagLayout()),
						panelButton = new JPanel(new FlowLayout());
	private GridBagConstraints l = new GridBagConstraints();
	private JButton cancel = new JButton("Cancelar"),
						  play = new JButton("Jogar");
	public static GuiTabuleiro gui;
	private JComboBox<String> peao1 = new JComboBox<String>(),
										  peao2 = new JComboBox<String>(),
										  peao3 = new JComboBox<String>(),
										  peao4 = new JComboBox<String>(),
										  peao5 = new JComboBox<String>();
	private JLabel nome1, nome2, nome3, nome4, nome5, ok0, ok1, ok2, ok3, ok4;
	private JTextField txt1 = new JTextField(10),
							 txt2 = new JTextField(10),
							 txt3 = new JTextField(10),
							 txt4 = new JTextField(10),
							 txt5 = new JTextField(10);
	
	public GuiAntesDeJogar(final int peoes, final String qntCasas, final String backG){
		super("Cores e Nomes dos Jogadores");
		setLayout(new GridBagLayout());
		
		setIconImage((new ImageIcon(getClass().getResource("/imagens/icone.png"))).getImage());
		
		l.insets = new Insets(10,5,0,5);
		l.gridx = 0;
		l.gridy = 0;
		panelOutros.add(new JLabel("Cor do pe�o 1:"), l);
		l.gridy = 1;
		panelOutros.add(new JLabel("Cor do pe�o 2:"), l);
		
		l.gridx = 1;
		l.gridy = 0;
		peao1.addItem("Verde");
		peao1.addItem("Amarelo");
		peao1.addItem("Azul");
		peao1.addItem("Branco");
		peao1.addItem("Roxo");
		panelOutros.add(peao1, l);
		peao1.addActionListener(this);
		
		l.gridy = 1;
		peao2.addItem("Verde");
		peao2.addItem("Amarelo");
		peao2.addItem("Azul");
		peao2.addItem("Branco");
		peao2.addItem("Roxo");
		peao2.setSelectedIndex(1);
		panelOutros.add(peao2, l);
		peao2.addActionListener(this);
		
		l.gridx = 2;
		l.gridy = 0;
		ok0 = new JLabel(new ImageIcon(getClass().getResource(peao("Verde"))));
		panelOutros.add(ok0, l);
		l.gridy = 1;
		ok1 = new JLabel(new ImageIcon(getClass().getResource(peao("Amarelo"))));
		panelOutros.add(ok1, l);
		
		l.gridx = 3;
		l.gridy = 0;
		nome1 = new JLabel ("Nome Jogador 1");
		panelOutros.add(nome1, l);
		
		l.gridy = 1;
		nome2 = new JLabel ("Nome Jogador 2");
		panelOutros.add(nome2, l);
		
		l.gridx = 4;
		l.gridy = 0;
		panelOutros.add(txt1, l);
		
		l.gridy = 1;
		panelOutros.add(txt2, l);
		
		outrosComponentes(peoes);
		
		cancel.addActionListener(this);
		panelButton.add(cancel);
		
		play.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e){
				try {
					if(txt1.getText().length() > 9 || txt2.getText().length() > 9 || txt3.getText().length() > 9 || txt4.getText().length() > 9 || txt5.getText().length() > 9)
						JOptionPane.showMessageDialog(null, "Nome dos jogadores s� pode ter at� 9 caracteres cada.", "Nome de Jogadores Inv�lido", 2);
					else {
						gui = new GuiTabuleiro(peoes, qntCasas, backG, vetorCor(peoes), vetorNome(peoes));
						dispose();
						PrincipalMain.menu.setVisible(false);
					}
				} catch (ExcecaoCorPeaoJaEscolhida e1) {
				} catch (ExcecaoNomePeaoJaEscolhido e1) {
				}
			}
		});
		panelButton.add(play);
		
		l.gridx = 0;
		l.gridy = 0;
		add(panelOutros, l);
		
		l.anchor = GridBagConstraints.EAST;
		l.gridy = 1;
		add(panelButton, l);
		
		setVisible(true);
		pack();
		setResizable(false);
		setLocationRelativeTo(null);
	}
	
	private void outrosComponentes(int peoes){
		if(peoes>=3){
			l.gridx = 0;
			l.gridy = 2;
			panelOutros.add(new JLabel("Cor do pe�o 3:"), l);
			l.gridx = 1;
			peao3.addItem("Verde");
			peao3.addItem("Amarelo");
			peao3.addItem("Azul");
			peao3.addItem("Branco");
			peao3.addItem("Roxo");
			panelOutros.add(peao3, l);
			peao3.setSelectedIndex(2);
			peao3.addActionListener(this);
			
			l.gridx = 2;
			ok2 = new JLabel(new ImageIcon(getClass().getResource(peao("Azul"))));
			panelOutros.add(ok2, l);
			
			l.gridx = 3;
			l.gridy = 2;
			nome3 = new JLabel ("Nome Jogador 3");
			panelOutros.add(nome3, l);
			
			l.gridx = 4;
			l.gridy = 2;
			panelOutros.add(txt3, l);
		}
		if(peoes>=4){
			l.gridx = 0;
			l.gridy = 3;
			panelOutros.add(new JLabel("Cor do pe�o 4:"), l);
			l.gridx = 1;
			peao4.addItem("Verde");
			peao4.addItem("Amarelo");
			peao4.addItem("Azul");
			peao4.addItem("Branco");
			peao4.addItem("Roxo");
			panelOutros.add(peao4, l);
			peao4.setSelectedIndex(3);
			peao4.addActionListener(this);
			
			l.gridx = 2;
			ok3 = new JLabel(new ImageIcon(getClass().getResource(peao("Branco"))));
			panelOutros.add(ok3, l);
			
			l.gridx = 3;
			l.gridy = 3;
			nome4 = new JLabel ("Nome Jogador 4");
			panelOutros.add(nome4, l);
			
			l.gridx = 4;
			l.gridy = 3;
			panelOutros.add(txt4, l);
		}
		if(peoes==5){
			l.gridx = 0;
			l.gridy = 4;
			panelOutros.add(new JLabel("Cor do pe�o 5:"), l);
			l.gridx = 1;
			peao5.addItem("Verde");
			peao5.addItem("Amarelo");
			peao5.addItem("Azul");
			peao5.addItem("Branco");
			peao5.addItem("Roxo");
			panelOutros.add(peao5, l);
			peao5.setSelectedIndex(4);
			peao5.addActionListener(this);
			
			l.gridx = 2;
			ok4 = new JLabel(new ImageIcon(getClass().getResource(peao("Roxo"))));
			panelOutros.add(ok4, l);
			
			l.gridx = 3;
			l.gridy = 4;
			nome5 = new JLabel ("Nome Jogador 5");
			panelOutros.add(nome5, l);
			
			l.gridx = 4;
			l.gridy = 4;
			panelOutros.add(txt5, l);
		}
	}
	private String peao(String peao){
		if(peao.equals("Verde"))
			return "/peoes/peaoVerde.png";
		else if(peao.equals("Amarelo"))
			return "/peoes/peaoAmarelo.png";
		else if(peao.equals("Azul"))
			return "/peoes/peaoAzul.png";
		else if(peao.equals("Branco"))
			return "/peoes/peaoBranco.png";
		else if(peao.equals("Roxo"))
			return "/peoes/peaoRoxo.png";
		else
			return null;
	}
	private String[] vetorCor(int peoes) throws ExcecaoCorPeaoJaEscolhida{
		
		String [] coresPeoes = new String[peoes];
		
		coresPeoes[0] = peao1.getItemAt(peao1.getSelectedIndex());
		coresPeoes[1] = peao2.getItemAt(peao2.getSelectedIndex());
		if(peoes>=3)
			coresPeoes[2] = peao3.getItemAt(peao3.getSelectedIndex());
		if(peoes>=4)
			coresPeoes[3] = peao4.getItemAt(peao4.getSelectedIndex());
		if(peoes==5)
			coresPeoes[4] = peao5.getItemAt(peao5.getSelectedIndex());
		
		String aux;
		int vezes = 0;
		for (int x=0; x<peoes; x++){
			aux = coresPeoes[x];
			
			for (int v=0; v<peoes; v++){
				if(aux == coresPeoes[v])
					vezes++;
			}
			if (vezes >= 2)
				throw new ExcecaoCorPeaoJaEscolhida();
			vezes = 0;
		}
		
		return coresPeoes;
	}
	private String[] vetorNome(int peoes) throws ExcecaoNomePeaoJaEscolhido{
		
		String [] nomePeoes = new String[peoes];
		
		nomePeoes[0] = txt1.getText();
		nomePeoes[1] = txt2.getText();
		if(peoes>=3)
			nomePeoes[2] = txt3.getText();
		if(peoes>=4)
			nomePeoes[3] = txt4.getText();
		if(peoes==5)
			nomePeoes[4] = txt5.getText();
		
		String aux;
		int vezes = 0;
		for (int x=0; x<peoes; x++){
			
			if(nomePeoes[x].equals(""))
				nomePeoes[x] = "Jogador "+(x+1);
			
			aux = nomePeoes[x];
			
			for (int v=0; v<peoes; v++){
				if(aux == nomePeoes[v])
					vezes++;
			}
			
			if (vezes >= 2)
				throw new ExcecaoNomePeaoJaEscolhido();
			vezes = 0;
		}
		
		return nomePeoes;
	}
	public void actionPerformed(ActionEvent e){
		
		if(e.getSource() == cancel)
			dispose();
		
		if(e.getSource() == peao1){
			if(peao1.getSelectedIndex() == 0)
				ok0.setIcon(new ImageIcon(getClass().getResource("/peoes/peaoVerde.png")));
			else if(peao1.getSelectedIndex() == 1)
				ok0.setIcon(new ImageIcon(getClass().getResource("/peoes/peaoAmarelo.png")));
			else if(peao1.getSelectedIndex() == 2)
				ok0.setIcon(new ImageIcon(getClass().getResource("/peoes/peaoAzul.png")));
			else if(peao1.getSelectedIndex() == 3)
				ok0.setIcon(new ImageIcon(getClass().getResource("/peoes/peaoBranco.png")));
			else if(peao1.getSelectedIndex() == 4)
				ok0.setIcon(new ImageIcon(getClass().getResource("/peoes/peaoRoxo.png")));
		}
		
		if(e.getSource() == peao2){
			if(peao2.getSelectedIndex() == 0)
				ok1.setIcon(new ImageIcon(getClass().getResource("/peoes/peaoVerde.png")));
			else if(peao2.getSelectedIndex() == 1)
				ok1.setIcon(new ImageIcon(getClass().getResource("/peoes/peaoAmarelo.png")));
			else if(peao2.getSelectedIndex() == 2)
				ok1.setIcon(new ImageIcon(getClass().getResource("/peoes/peaoAzul.png")));
			else if(peao2.getSelectedIndex() == 3)
				ok1.setIcon(new ImageIcon(getClass().getResource("/peoes/peaoBranco.png")));
			else if(peao2.getSelectedIndex() == 4)
				ok1.setIcon(new ImageIcon(getClass().getResource("/peoes/peaoRoxo.png")));
		}
		
		if(e.getSource() == peao3){
			if(peao3.getSelectedIndex() == 0)
				ok2.setIcon(new ImageIcon(getClass().getResource("/peoes/peaoVerde.png")));
			else if(peao3.getSelectedIndex() == 1)
				ok2.setIcon(new ImageIcon(getClass().getResource("/peoes/peaoAmarelo.png")));
			else if(peao3.getSelectedIndex() == 2)
				ok2.setIcon(new ImageIcon(getClass().getResource("/peoes/peaoAzul.png")));
			else if(peao3.getSelectedIndex() == 3)
				ok2.setIcon(new ImageIcon(getClass().getResource("/peoes/peaoBranco.png")));
			else if(peao3.getSelectedIndex() == 4)
				ok2.setIcon(new ImageIcon(getClass().getResource("/peoes/peaoRoxo.png")));
		}
		
		if(e.getSource() == peao4){
			if(peao4.getSelectedIndex() == 0)
				ok3.setIcon(new ImageIcon(getClass().getResource("/peoes/peaoVerde.png")));
			else if(peao4.getSelectedIndex() == 1)
				ok3.setIcon(new ImageIcon(getClass().getResource("/peoes/peaoAmarelo.png")));
			else if(peao4.getSelectedIndex() == 2)
				ok3.setIcon(new ImageIcon(getClass().getResource("/peoes/peaoAzul.png")));
			else if(peao4.getSelectedIndex() == 3)
				ok3.setIcon(new ImageIcon(getClass().getResource("/peoes/peaoBranco.png")));
			else if(peao4.getSelectedIndex() == 4)
				ok3.setIcon(new ImageIcon(getClass().getResource("/peoes/peaoRoxo.png")));
		}
		
		if(e.getSource() == peao5){
			if(peao5.getSelectedIndex() == 0)
				ok4.setIcon(new ImageIcon(getClass().getResource("/peoes/peaoVerde.png")));
			else if(peao5.getSelectedIndex() == 1)
				ok4.setIcon(new ImageIcon(getClass().getResource("/peoes/peaoAmarelo.png")));
			else if(peao5.getSelectedIndex() == 2)
				ok4.setIcon(new ImageIcon(getClass().getResource("/peoes/peaoAzul.png")));
			else if(peao5.getSelectedIndex() == 3)
				ok4.setIcon(new ImageIcon(getClass().getResource("/peoes/peaoBranco.png")));
			else if(peao5.getSelectedIndex() == 4)
				ok4.setIcon(new ImageIcon(getClass().getResource("/peoes/peaoRoxo.png")));
		}
			
	}
}