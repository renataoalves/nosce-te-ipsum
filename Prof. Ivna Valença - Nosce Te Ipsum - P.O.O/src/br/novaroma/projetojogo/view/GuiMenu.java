package br.novaroma.projetojogo.view;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import br.novaroma.projetojogo.controller.ControllerAlteracao;

public class GuiMenu extends GuiBarra{
	private static final long serialVersionUID = 1L;
	
	private JButton iniciar = new JButton("Iniciar Jogo");
	private JPanel padrao = new JPanel(new BorderLayout()),
						 painelImagem = new JPanel(),
						 painelBotao = new JPanel();
	private JLabel labelImagem = new JLabel(new ImageIcon(getClass().getResource("/imagens/menu.png")));
	public GuiMenu(){
		
		painelImagem.add(labelImagem);
		
		iniciar.setToolTipText("Alt + J");
		iniciar.setMnemonic(KeyEvent.VK_J);
		iniciar.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				ControllerAlteracao.escolherJogo();
			}
		});
		
		painelBotao.add(iniciar);

		padrao.add(painelImagem, BorderLayout.CENTER);
		padrao.add(painelBotao, BorderLayout.SOUTH);
		
		add(padrao);
		setResizable(false);
		setSize(745, 340);
		setLocationRelativeTo(null);
	}
	
}