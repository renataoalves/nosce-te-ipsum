package br.novaroma.projetojogo.view;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.swing.*;

import br.novaroma.projetojogo.controller.ControllerCadastro;
import br.novaroma.projetojogo.controller.ControllerJogar;
import br.novaroma.projetojogo.excecoes.ExcecaoJogoJaExiste;
import br.novaroma.projetojogo.excecoes.ExcecaoNomeInvalido;
import br.novaroma.projetojogo.excecoes.ExcecaoNomeNaoInformado;

public class GuiAlteracao extends JFrame implements ActionListener{
	private static final long serialVersionUID = 1L;
	
	private JPanel outro = new JPanel(new GridBagLayout()),
						qntCasas = new JPanel(new GridBagLayout()),
						qntPeoes = new JPanel(new GridBagLayout()),
						panelButtons = new JPanel(new FlowLayout());;
	private GridBagConstraints l = new GridBagConstraints();
	private JTextField txtNome, txtDiretorio;
	private JRadioButton qnt36 = new JRadioButton("36 casas"),
			 					 qnt32 = new JRadioButton("32 casas"),
			 					 qnt28 = new JRadioButton("28 casas");
	private ButtonGroup grupo = new ButtonGroup();
	private JComboBox<Integer> comboBoxPeoes = new JComboBox<Integer>();
	private JLabel drawPeao = new JLabel(), planoDeFundo;
	private JButton backg = new JButton("Procurar Novo"), padrao,
						  old = new JButton("Antiga imagem"),
						  back = new JButton("Cancelar"),
						  alter = new JButton("Alterar");
	private JFileChooser fc = new JFileChooser();
	private String nome;
	
	public GuiAlteracao(String nome, String casas, int peoes, final String diretorio){
		super("Altera��o de Componentes do Jogo: "+nome);
		setLayout(new GridBagLayout());
		
		this.nome = nome;
		setIconImage((new ImageIcon(getClass().getResource("/imagens/icone.png"))).getImage());
		
		l.insets = new Insets(2,2,3,0);
		l.gridx = 0;
		l.gridy = 0;
		l.anchor = GridBagConstraints.EAST;
		outro.add(new JLabel("Nome: "), l);
		l.gridx = 1;
		l.anchor = GridBagConstraints.WEST;
		outro.add(txtNome = new JTextField(nome, 15), l);
		
		selecaoBotao(casas);
		grupo.add(qnt36);
		grupo.add(qnt32);
		grupo.add(qnt28);
		
		l.gridx = 1;
		qntCasas.add(qnt36, l);
		l.gridx = 2;
		qntCasas.add(qnt32, l);
		l.gridx = 3;
		qntCasas.add(qnt28, l);

		l.gridx = 0;
		l.gridy = 1;
		l.anchor = GridBagConstraints.EAST;
		outro.add(new JLabel("Quantidade de casas: "), l);
		l.gridx = 1;
		l.anchor = GridBagConstraints.WEST;
		outro.add(qntCasas, l);
		
		comboBoxPeoes.addItem(2);
		comboBoxPeoes.addItem(3);
		comboBoxPeoes.addItem(4);
		comboBoxPeoes.addItem(5);
		quantEscolhidaPeao(peoes);
		comboBoxPeoes.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if(comboBoxPeoes.getSelectedIndex() == 0)
					drawPeao.setIcon(new ImageIcon(getClass().getResource("/peoes/2peoes.png")));
				else if(comboBoxPeoes.getSelectedIndex() == 1)
					drawPeao.setIcon(new ImageIcon(getClass().getResource("/peoes/3peoes.png")));
				else if(comboBoxPeoes.getSelectedIndex() == 2)
					drawPeao.setIcon(new ImageIcon(getClass().getResource("/peoes/4peoes.png")));
				else 
					drawPeao.setIcon(new ImageIcon(getClass().getResource("/peoes/5peoes.png")));
			}
		});

		l.gridx = 0;
		l.gridy = 2;
		l.anchor = GridBagConstraints.EAST;
		outro.add(new JLabel("Quantidade de pe�es: "), l);
		
		l.insets = new Insets(0,5,0,5);
		l.anchor = GridBagConstraints.WEST;
		l.gridx = 0;
		l.gridy = 0;
		qntPeoes.add(comboBoxPeoes, l);
		l.gridx = 1;
		qntPeoes.add(drawPeao, l);
		
		l.insets = new Insets(2,2,3,0);
		l.gridx = 1;
		l.gridy = 2;
		outro.add(qntPeoes, l);
		
		planoDeFundoEscolhido(diretorio);		
		l.gridx = 0;
		l.gridy = 3;
		l.anchor = GridBagConstraints.EAST;
		outro.add(new JLabel("Plano de Fundo: "), l);
		l.anchor = GridBagConstraints.WEST;
		l.gridx = 1;
		outro.add(txtDiretorio = new JTextField(diretorio, 25), l);
		txtDiretorio.setEnabled(false);
		txtDiretorio.setHorizontalAlignment(JTextField.CENTER);
		
		fc.setAcceptAllFileFilterUsed(false);
		fc.setDialogTitle("Procurar imagem para background");
		fc.setFileFilter(new ExtensionFileFilter("Imagem (.png) (.jpg)", "png", "jpg"));
		
		l.insets = new Insets(0,20,0,0);
		l.anchor = GridBagConstraints.WEST;
		l.gridy = 4;
		outro.add(backg, l);
		l.insets = new Insets(0,0,0,20);
		l.anchor = GridBagConstraints.EAST;
		outro.add(old, l);
		old.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				txtDiretorio.setText(diretorio);
				if(diretorio.equals("Azul Claro"))
					planoDeFundo.setIcon(new ImageIcon(redimensionar(200, 200, new ImageIcon(getClass().getResource("/imagens/azul.png")).getImage())));
				else 
					planoDeFundo.setIcon(new ImageIcon(redimensionar(200, 200, new ImageIcon(diretorio).getImage())));
			}
		});
		
		back.addActionListener(this);
		panelButtons.add(back);

		alter.addActionListener(this);
		panelButtons.add(alter);
		
		l.insets = new Insets(0,0,0,0);
		l.anchor = GridBagConstraints.WEST;
		l.gridx = 0;
		l.gridy = 0;
		add(outro, l);
		l.insets = new Insets(5,5,0,0);
		l.anchor = GridBagConstraints.EAST;
		l.gridx = 3;
		l.gridy = 6;
		add(panelButtons, l);
		l.anchor = GridBagConstraints.SOUTH;
		l.gridx = 2;
		l.gridy = 0;
		padrao = new JButton(new ImageIcon(getClass().getResource("/imagens/lixeira.png")));
		padrao.setToolTipText("Op��o para definir o plano de fundo com a cor Azul Claro");
		add(padrao, l);
		l.anchor = GridBagConstraints.CENTER;
		l.gridx = 3;
		l.gridy = 0;
		add(planoDeFundo, l);
		
		padrao.addActionListener(this);
		backg.addActionListener(this);
		
		setVisible(true);
		setResizable(false);
		setSize(700, 285);
		setLocationRelativeTo(null);
	}
	
	private void selecaoBotao(String casas){
		if(casas.equals("36 casas"))
			qnt36.setSelected(true);
		if(casas.equals("32 casas"))
			qnt32.setSelected(true);
		else
			qnt28.setSelected(true);
	}
	private void quantEscolhidaPeao(int peoes){
		if(peoes == 2){
			drawPeao.setIcon(new ImageIcon(getClass().getResource("/peoes/2peoes.png")));
			comboBoxPeoes.setSelectedIndex(0);
		} else if(peoes == 3){
			drawPeao.setIcon(new ImageIcon(getClass().getResource("/peoes/3peoes.png")));
			comboBoxPeoes.setSelectedIndex(1);
		} else if(peoes == 4){
			drawPeao.setIcon(new ImageIcon(getClass().getResource("/peoes/4peoes.png")));
			comboBoxPeoes.setSelectedIndex(2);
		} else {
			drawPeao.setIcon(new ImageIcon(getClass().getResource("/peoes/5peoes.png")));
			comboBoxPeoes.setSelectedIndex(3);
		}
	}
	private void planoDeFundoEscolhido(String diretorio){
		if(diretorio.equals("Azul Claro"))
			planoDeFundo = new JLabel(new ImageIcon(redimensionar(200, 200, new ImageIcon(getClass().getResource("/imagens/azul.png")).getImage())));
		else
			planoDeFundo = new JLabel(new ImageIcon(redimensionar(200, 200, new ImageIcon(diretorio).getImage())));
	}
	private Image redimensionar(int x, int y, Image imagem){
		BufferedImage render = new BufferedImage (x, y, BufferedImage.TYPE_INT_RGB); // rederizador

	    Graphics2D g2 = render.createGraphics (); //criar objeto em 2d
	    g2.setRenderingHint (RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
	    g2.drawImage(imagem, 0, 0, x, y, null); //cria imagem dimensionada
	    g2.dispose (); // limpa a imagem anterior para liberar outra
	    
	    return render;
	 }
	public void actionPerformed(ActionEvent e) {
		
		if(e.getSource() == backg){
			if (fc.showSaveDialog(this) == JFileChooser.APPROVE_OPTION){
				File arquivo = fc.getSelectedFile();
				String diretorioArquivo = arquivo.getPath();
				txtDiretorio.setText(diretorioArquivo);
				planoDeFundo.setIcon(new ImageIcon(redimensionar(200, 200, new ImageIcon(diretorioArquivo).getImage())));
			}
		}
		
		if(e.getSource() == padrao){
			txtDiretorio.setText("Azul Claro");
			planoDeFundo.setIcon(new ImageIcon(redimensionar(200, 200, new ImageIcon(getClass().getResource("/imagens/azul.png")).getImage())));
		}
		
		if(e.getSource() == alter){
			try {
				if(txtNome.getText().length() > 15)
					JOptionPane.showMessageDialog(null, "Nome de jogo s� pode ter at� 15 caracteres.", "Nome de Jogo Inv�lido", 2);
				 else {
					String casas = null;
					if(qnt36.isSelected() == true)
						casas = "36 casas";
					else if(qnt32.isSelected() == true)
						casas = "32 casas";
					else
						casas = "28 casas";

					nome  = txtNome.getText().trim();
					txtNome.setText(txtNome.getText().trim());
					ControllerCadastro.verificarNome(nome);
					ControllerCadastro.getGames().get(ControllerJogar.getIndiceJogoEscolhido()).setNomeJogo(nome);
					ControllerCadastro.getGames().get(ControllerJogar.getIndiceJogoEscolhido()).setQntCasas(casas);
					ControllerCadastro.getGames().get(ControllerJogar.getIndiceJogoEscolhido()).setQntPeoes(comboBoxPeoes.getSelectedIndex()+2);
					ControllerCadastro.getGames().get(ControllerJogar.getIndiceJogoEscolhido()).setBackground(txtDiretorio.getText());
					dispose();
					JOptionPane.showMessageDialog(null, "Altera��es realizadas com sucesso!", "Altera��o de Componentes do Jogo", 1);
				}
			} catch (ExcecaoNomeNaoInformado ex) {
				JOptionPane.showMessageDialog(null, "Nome de jogo n�o informado. \n\nJogo n�o alterado!", "Altera��o de Componentes de Jogo", 2);
			} catch (ExcecaoJogoJaExiste ex) {
				JOptionPane.showMessageDialog(null, "Nome de jogo j� existe. \n\nJogo n�o alterado!", "Altera��o de Componentes de Jogo", 2);
			} catch (ExcecaoNomeInvalido ex) {
			}
		}
		
		if(e.getSource() == back)
			dispose();
		
	}
}