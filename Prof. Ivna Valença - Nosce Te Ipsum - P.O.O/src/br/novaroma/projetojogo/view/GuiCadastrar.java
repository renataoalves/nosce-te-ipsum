package br.novaroma.projetojogo.view;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Random;

import javax.swing.*;

import br.novaroma.projetojogo.controller.ControllerCadastro;
import br.novaroma.projetojogo.excecoes.ExcecaoJogoJaExiste;
import br.novaroma.projetojogo.excecoes.ExcecaoNomeInvalido;
import br.novaroma.projetojogo.excecoes.ExcecaoNomeNaoInformado;

public class GuiCadastrar extends JFrame implements ActionListener{
	private static final long serialVersionUID = 1L;
	
	private static JTextField txtNome = new JTextField(12),
									  diretorio = new JTextField("Azul Claro", 12);
	private JFileChooser fc = new JFileChooser();
	private static JRadioButton qnt36 = new JRadioButton("36 casas"),
							   			  qnt32	= new JRadioButton("32 casas"),
							   			  qnt28 = new JRadioButton("28 casas"),
							   			  casaPredefinida = new JRadioButton("Predefinidas"),
							   			  casaAutomatica = new JRadioButton("Autom�tico");
	private ButtonGroup grupoCasas = new ButtonGroup(),
					 			 grupoEspeciais = new ButtonGroup();
	private JButton backg = new JButton("Procurar"), padrao,
			   			  cancel = new JButton("Cancelar"),
			   			  register = new JButton("Cadastrar"),
			   			  startEnd1 = new JButton("", new ImageIcon(getClass().getResource("/imagens/saida.png"))),
			   			  startEnd2 = new JButton("", new ImageIcon(getClass().getResource("/imagens/saida.png")));
	private JPanel painelLabels = new JPanel(new GridBagLayout()),
						 panelInformacoes = new JPanel(new GridBagLayout()),
		     			 panelButton = new JPanel(new GridBagLayout()),
		     			 panelGroupButton = new JPanel(new GridBagLayout()),
		     			 panelGroupButtonEspeciais = new JPanel(new GridBagLayout()),
		     			 panelTabuleiro = new JPanel(new GridBagLayout());
	private static JComboBox<Integer> qntPeoes = new JComboBox<Integer>();
	private GridBagConstraints l = new GridBagConstraints();
	private JButton casas[][] = new JButton[8][8];
	private String casaAcao[] = new String[10];
	private int qntCasas = 1;
	private Object[] acoes = {"Pular uma casa", "Ficar sem jogar uma rodada", "Jogue outra vez"};
	private JLabel informacao = new JLabel("OBS: Escolha 5 casas, clique e defina a a��o que ter� quando o pe�o parar por cima.");
	
	public GuiCadastrar(){
		super("Cadastro de Jogo");
		setLayout(new GridBagLayout());
		
		setIconImage((new ImageIcon(getClass().getResource("/imagens/icone.png"))).getImage());
		
		l.anchor = GridBagConstraints.EAST;
		l.gridx = 0;
		l.gridy = 0;
		painelLabels.add(new JLabel("Nome do jogo: *"), l);
		
		l.insets = new Insets(15,0,0,0);
		l.gridy = 1;
		painelLabels.add(new JLabel("Casas Especiais:"), l);

		l.gridy = 2;
		painelLabels.add(new JLabel("Quantidade de pe�es:"), l);
		
		l.gridy = 3;
		painelLabels.add(new JLabel("Quantidade de casas:"), l);
		
		l.gridy = 4;
		painelLabels.add(new JLabel("Plano de fundo: "), l);
		
		l.insets = new Insets(7,5,0,0);
		l.gridy = 0;
		l.anchor = GridBagConstraints.EAST;
		panelInformacoes.add(new JLabel("*Campos"), l);
		
		l.anchor = GridBagConstraints.WEST;
		panelInformacoes.add(txtNome, l);
		
		casaAutomatica.setSelected(true);
		grupoEspeciais.add(casaPredefinida);
		grupoEspeciais.add(casaAutomatica);
		panelGroupButtonEspeciais.add(casaPredefinida);
		panelGroupButtonEspeciais.add(casaAutomatica);
		l.insets = new Insets(7,10,0,0);
		l.gridy = 1;
		panelInformacoes.add(panelGroupButtonEspeciais, l);
		
		qntPeoes.addItem(2);
		qntPeoes.addItem(3);
		qntPeoes.addItem(4);
		qntPeoes.addItem(5);
		l.gridy = 2;
		panelInformacoes.add(qntPeoes,l);
		
		qnt36.setSelected(true);
		grupoCasas.add(qnt36);
		grupoCasas.add(qnt32);
		grupoCasas.add(qnt28);
		panelGroupButton.add(qnt36);
		panelGroupButton.add(qnt32);
		panelGroupButton.add(qnt28);
		l.insets = new Insets(7,10,0,0);
		l.gridy = 3;
		panelInformacoes.add(panelGroupButton, l);
		
		l.gridy = 4;
		diretorio.setEditable(false);
		diretorio.setHorizontalAlignment(JTextField.CENTER);
		panelInformacoes.add(diretorio,l);
		
		fc.setAcceptAllFileFilterUsed(false);
		fc.setDialogTitle("Procurar imagem para background");
		fc.setFileFilter(new ExtensionFileFilter("Imagem (.png) (.jpg)", "png", "jpg"));
		
		l.anchor = GridBagConstraints.EAST;
		l.gridy = 4;
		panelInformacoes.add(backg,l);
		
		padrao = new JButton(new ImageIcon(getClass().getResource("/imagens/lixeira.png")));
		padrao.setToolTipText("Op��o para definir o plano de fundo com a cor Azul Claro");
		l.gridx = 1;
		l.gridy = 4;
		panelInformacoes.add(padrao, l);
		
		l.gridy = 0;
		l.insets = new Insets(7,0,0,0);
		l.anchor = GridBagConstraints.WEST;
		panelInformacoes.add(new JLabel(" Obrigat�rio"), l);
		
		l.insets = new Insets(7,10,0,0);
		l.anchor = GridBagConstraints.EAST;
		
		qnt36.addActionListener(this);
		qnt32.addActionListener(this);
		qnt28.addActionListener(this);
		casaPredefinida.addActionListener(this);
		casaAutomatica.addActionListener(this);
		backg.addActionListener(this);
		padrao.addActionListener(this);
		register.addActionListener(this);
		cancel.addActionListener(this);
		
		l.gridx = 0;
		l.gridy = 0;
		panelButton.add(cancel, l);
		l.gridx = 1;
		panelButton.add(register, l);
		
		JPanel outro = new JPanel(new GridBagLayout());
		l.insets = new Insets(5,0,0,0);
		l.gridx = 0;
		l.gridy = 1;
		outro.add(painelLabels, l);
		l.gridx = 1;
		outro.add(panelInformacoes, l);
		
		l.gridx = 0;
		l.gridy = 0;
		add(outro, l);
		
		l.anchor = GridBagConstraints.EAST;
		l.gridx = 0;
		l.gridy = 1;
		add(panelButton, l);
		
		setSize(500, 245);
		setVisible(true);
		setResizable(false);
		setLocationRelativeTo(null);
	}
	
	private void limparComponentes(){
		txtNome.setText("");
		qntPeoes.removeAllItems();
		diretorio.setText("Azul Claro");
		casaAutomatica.setSelected(true);
		qnt36.setSelected(true);
	}
	public void actionPerformed(ActionEvent e) {
		
		if(e.getSource() == backg){
			if (fc.showSaveDialog(this) == JFileChooser.APPROVE_OPTION){
		        File arquivo = fc.getSelectedFile();
		        String diretorioArquivo = arquivo.getPath();
		        diretorio.setText(diretorioArquivo);
			}
		}
		
		if(e.getSource() == padrao)
			diretorio.setText("Azul Claro");
		
		if(e.getSource() == cancel){
			dispose();
			limparComponentes();
		}
		
		if(e.getSource() == register){
			try{
				if(txtNome.getText().length() > 15)
					JOptionPane.showMessageDialog(null, "Nome de jogo s� pode ter at� 15 caracteres.", "Nome de Jogo Inv�lido", 2);
				else if(txtNome.getText().equals(""))
					JOptionPane.showMessageDialog(null, "Nome de jogo n�o informado.", "Nome de Jogo Inv�lido", 2);
				else {
					String casas = null;
					if(qnt36.isSelected() == true)
						casas = "36 casas";
					else if(qnt32.isSelected() == true)
						casas = "32 casas";
					else if(qnt28.isSelected() == true)
						casas = "28 casas";
					
					dispose();
					ControllerCadastro.cadastrarJogo(txtNome.getText(), casas, qntPeoes.getSelectedIndex()+2, diretorio.getText(), casasEspeciais(casaAcao));
					limparComponentes();
				}
			} catch (ExcecaoNomeNaoInformado ex){
				JOptionPane.showMessageDialog(null, "Nome n�o informado. Informe-o.", "Cadastro de Jogo", 2);
			} catch (ExcecaoJogoJaExiste ex){
				txtNome.setText(txtNome.getText().trim());
				JOptionPane.showMessageDialog(null, "J� existe um outro jogo com esse nome. Cadastre um diferente!", "Cadastro de Jogo", 2);
			} catch (ExcecaoNomeInvalido ex){
				txtNome.setText(txtNome.getText().trim());
			}
		}
		
		if(e.getSource() == casaAutomatica){
			setSize(500, 245);
			setLocationRelativeTo(null);
			remove(informacao);
			panelTabuleiro.removeAll();
			remove(panelTabuleiro);
			revalidate();
			
			casaAcao[0] = null;
			
			register.setEnabled(true);
			panelButton.removeAll();
			remove(panelButton);
			revalidate();
			l.anchor = GridBagConstraints.EAST;
			l.gridx = 0;
			l.insets = new Insets(5,6,0,0);
			l.gridy = 1;
			panelButton.add(cancel, l);
			l.gridx = 1;
			panelButton.add(register, l);
			l.gridx = 0;
			l.gridy = 1;
			add(panelButton, l);
			revalidate();
		}
		
		tabuleirosCasasEspeciais(e);
		
		if(e.getSource() == casaPredefinida){
			if(qnt36.isSelected() == true || qnt32.isSelected() == true || qnt28.isSelected() == true){
				
				register.setEnabled(false);
				
		    	if(qnt36.isSelected() == true)
		    		tabuleiroCom36();
		    	
		    	if(qnt32.isSelected() == true)
		    		tabuleiroCom32();
		    	
		    	if(qnt28.isSelected() == true)
		    		tabuleiroCom28();
		    	
				l.gridx = 0;
				l.gridy = 2;
				add(informacao, l);
				
				l.anchor = GridBagConstraints.CENTER;
				l.gridy = 3;
				add(panelTabuleiro, l);
				
				l.gridx = 0;
				l.gridy = 0;
				l.anchor = GridBagConstraints.CENTER;
				panelButton.add(new JLabel("Somente depois de definir a a��o das casas, clique em \"Cadastrar\""), l);
				l.anchor = GridBagConstraints.EAST;
				l.insets = new Insets(0,5,0,0);
				l.gridy = 1;
				l.gridx = 0;
				panelButton.add(cancel, l);
				l.gridx = 1;
				panelButton.add(register, l);
				l.gridx = 0;
				l.gridy = 4;
				add(panelButton, l);
				
				setLocationRelativeTo(null);
				panelTabuleiro.setBorder(BorderFactory.createEtchedBorder(1));
				panelTabuleiro.revalidate();
				revalidate();
			}
		}
		
		if(e.getSource() == qnt36){
			if(casaPredefinida.isSelected() == true)
				tabuleiroCom36();
		}
		
		if(e.getSource() == qnt32){
			if(casaPredefinida.isSelected() == true)
				tabuleiroCom32();
		}
		
		if(e.getSource() == qnt28){
			if(casaPredefinida.isSelected() == true)
				tabuleiroCom28();
		}
	}
	private void tabuleiroCom36(){
		panelTabuleiro.removeAll();
		l.insets = new Insets(1,1,1,1);
		l.anchor = GridBagConstraints.CENTER;
		for (int x = 0; x < 8; x++) {
		    for (int y = 0; y < 8; y++) {
		    	if(x==0 && y==7){
		    		l.gridx = x;
	        		l.gridy = y;
	        		casas[x][y] = startEnd1;
	        		startEnd1.setEnabled(false);
	        		panelTabuleiro.add(casas[x][y], l);
		    	}else if(x==0 && y!=7){
		        	l.gridx = x;
	        		l.gridy = y;
	        		casas[x][y] = new JButton(""+(7-y));
	        		panelTabuleiro.add(casas[x][y], l);
		        } else if (x==1 && y==0){
		        	l.gridx = x;
	        		l.gridy = y;
	        		casas[x][y] = new JButton("8");
	        		panelTabuleiro.add(casas[x][y], l);	
		        } else if (x==2){
		        	l.gridx = x;
	        		l.gridy = y;
	        		casas[x][y] = new JButton(""+(9+y));
	        		panelTabuleiro.add(casas[x][y], l);
		        } else if(x==3 && y==7){
		        	l.gridx = x;
	        		l.gridy = y;
	        		casas[x][y] = new JButton("17");
	        		panelTabuleiro.add(casas[x][y], l);
		        } else if (x==4){
		        	l.gridx = x;
	        		l.gridy = y;
	        		casas[x][y] = new JButton(""+(25-y));
	        		panelTabuleiro.add(casas[x][y], l);
		        } else if(x==5 && y==0){
		        	l.gridx = x;
	        		l.gridy = y;
	        		casas[x][y] = new JButton("26");
	        		panelTabuleiro.add(casas[x][y], l);
		        } else if(x==6){
		        	l.gridx = x;
	        		l.gridy = y;
	        		casas[x][y] = new JButton(""+(27+y));
	        		panelTabuleiro.add(casas[x][y], l);
		        } else if(x==7 && y==7 && y!=6){
		        	l.gridx = x;
	        		l.gridy = y;
	        		casas[x][y] = new JButton("35");
	        		panelTabuleiro.add(casas[x][y], l);
		        } else if(x==7 && y==6){
		        	l.gridx = x;
	        		l.gridy = y;
	        		casas[x][y] = startEnd2;
	        		startEnd2.setEnabled(false);
	        		panelTabuleiro.add(casas[x][y], l);
		        }
		    }
		}
		
		for (int a = 0; a < 8; a++) {
		    for (int b = 0; b < 8; b++){
		    	if(a==0  && b!=7 || a==1 && b==0 || a==2 || a==3 && b==7 || a==4 || a==5 && b==0 || a==6 || a==7 && b==7)
		    		casas[a][b].addActionListener(this);
		    }
		}
		
		l.insets = new Insets(5,0,0,0);
		l.anchor = GridBagConstraints.SOUTHWEST;
		l.gridx = 0;
		l.gridy = 2;
		add(informacao, l);
		
		l.anchor = GridBagConstraints.CENTER;
		l.gridy = 3;
		add(panelTabuleiro, l);
		
		l.anchor = GridBagConstraints.EAST;
		l.gridy = 4;
		add(panelButton, l);
		
		setSize(530, 515);
		setLocationRelativeTo(null);
		panelTabuleiro.setBorder(BorderFactory.createEtchedBorder(1));
		panelTabuleiro.revalidate();
		revalidate();
	}
	private void tabuleiroCom32(){
		panelTabuleiro.removeAll();
		l.insets = new Insets(1,1,1,1);
		l.anchor = GridBagConstraints.CENTER;
		for (int x = 0; x < 8; x++) {
		    for (int y = 0; y < 8; y++) {
		    	if(x==0 && y==7){
		    		l.gridx = x;
	        		l.gridy = y;
	        		casas[x][y] = startEnd1;
	        		startEnd1.setEnabled(false);
	        		panelTabuleiro.add(casas[x][y], l);
		    	}else if(x==0 && y!=7 && y!=0){
		        	l.gridx = x;
	        		l.gridy = y;
	        		casas[x][y] = new JButton(""+(7-y));
	        		panelTabuleiro.add(casas[x][y], l);
		        } else if (x==1 && y==1){
		        	l.gridx = x;
	        		l.gridy = y;
	        		casas[x][y] = new JButton("7");
	        		panelTabuleiro.add(casas[x][y], l);	
		        } else if (x==2 && y!=0){
		        	l.gridx = x;
	        		l.gridy = y;
	        		casas[x][y] = new JButton(""+(7+y));
	        		panelTabuleiro.add(casas[x][y], l);
		        } else if(x==3 && y==7){
		        	l.gridx = x;
	        		l.gridy = y;
	        		casas[x][y] = new JButton("15");
	        		panelTabuleiro.add(casas[x][y], l);
		        } else if (x==4 && y!=0){
		        	l.gridx = x;
	        		l.gridy = y;
	        		casas[x][y] = new JButton(""+(23-y));
	        		panelTabuleiro.add(casas[x][y], l);
		        } else if(x==5 && y==1){
		        	l.gridx = x;
	        		l.gridy = y;
	        		casas[x][y] = new JButton("23");
	        		panelTabuleiro.add(casas[x][y], l);
		        } else if(x==6 && y!=0){
		        	l.gridx = x;
	        		l.gridy = y;
	        		casas[x][y] = new JButton(""+(23+y));
	        		panelTabuleiro.add(casas[x][y], l);
		        } else if(x==7 && y==7 && y!=6){
		        	l.gridx = x;
	        		l.gridy = y;
	        		casas[x][y] = new JButton("31");
	        		panelTabuleiro.add(casas[x][y], l);
		        } else if(x==7 && y==6){
		        	l.gridx = x;
	        		l.gridy = y;
	        		casas[x][y] = startEnd2;
	        		startEnd2.setEnabled(false);
	        		panelTabuleiro.add(casas[x][y], l);
		        }
		    }
		}
		
		for (int a = 0; a < 8; a++) {
		    for (int b = 0; b < 8; b++){
		    	if(a==0 && b!=0 || a==1 && b==1 || a==2 && b!=0 || a==3 && b==7 || a==4 && b!=0 || a==5 && b==1 || a==6 && b!=0 || a==7 && b==7)
		    		casas[a][b].addActionListener(this);
		    }
		}
		
		l.insets = new Insets(5,0,0,0);
		l.gridx = 0;
		l.gridy = 2;
		add(informacao, l);
		
		l.anchor = GridBagConstraints.CENTER;
		l.gridy = 3;
		add(panelTabuleiro, l);
		
		l.anchor = GridBagConstraints.EAST;
		l.gridy = 4;
		add(panelButton, l);
		
		setSize(530, 495);
		setLocationRelativeTo(null);
		panelTabuleiro.setBorder(BorderFactory.createEtchedBorder(1));
		informacao.revalidate();
		panelTabuleiro.revalidate();
		revalidate();
	}
	private void tabuleiroCom28(){
		panelTabuleiro.removeAll();
		l.insets = new Insets(1,1,1,1);
		l.anchor = GridBagConstraints.CENTER;
		for (int x = 0; x < 8; x++) {
		    for (int y = 0; y < 8; y++) {
		    	if(x==0 && y==6){
		    		l.gridx = x;
	        		l.gridy = y;
	        		casas[x][y] = startEnd1;
	        		startEnd1.setEnabled(false);
	        		panelTabuleiro.add(casas[x][y], l);
		    	}else if(x==0 && y!=0 && y!=7){
		        	l.gridx = x;
	        		l.gridy = y;
	        		casas[x][y] = new JButton(""+(6-y));
	        		panelTabuleiro.add(casas[x][y], l);
		        } else if (x==1 && y==1){
		        	l.gridx = x;
	        		l.gridy = y;
	        		casas[x][y] = new JButton("6");
	        		panelTabuleiro.add(casas[x][y], l);	
		        } else if (x==2 && y!=0 && y!=7){
		        	l.gridx = x;
	        		l.gridy = y;
	        		casas[x][y] = new JButton(""+(6+y));
	        		panelTabuleiro.add(casas[x][y], l);
		        } else if(x==3 && y==6){
		        	l.gridx = x;
	        		l.gridy = y;
	        		casas[x][y] = new JButton("13");
	        		panelTabuleiro.add(casas[x][y], l);
		        } else if (x==4 && y!=0 && y!=7){
		        	l.gridx = x;
	        		l.gridy = y;
	        		casas[x][y] = new JButton(""+(20-y));
	        		panelTabuleiro.add(casas[x][y], l);
		        } else if(x==5 && y==1){
		        	l.gridx = x;
	        		l.gridy = y;
	        		casas[x][y] = new JButton("20");
	        		panelTabuleiro.add(casas[x][y], l);
		        } else if(x==6 && y!=0 && y!=7){
		        	l.gridx = x;
	        		l.gridy = y;
	        		casas[x][y] = new JButton(""+(20+y));
	        		panelTabuleiro.add(casas[x][y], l);
		        } else if(x==7 && y==6 && y!=5){
		        	l.gridx = x;
	        		l.gridy = y;
	        		casas[x][y] = new JButton("27");
	        		panelTabuleiro.add(casas[x][y], l);
		        } else if(x==7 && y==5){
		        	l.gridx = x;
	        		l.gridy = y;
	        		casas[x][y] = startEnd2;
	        		startEnd2.setEnabled(false);
	        		panelTabuleiro.add(casas[x][y], l);
		        } 
		    }
		}
		
		for (int a = 0; a < 8; a++) {
		    for (int b = 0; b < 8; b++){
		    	if(a==0 && b != 6 && b!=0 && b!=7 || a==1 && b==1 || a==2 && b!=0 && b!=7 || a==3 && b==6 || a==4 && b!=0 && b!=7 || a==5 && b==1 || a==6 && b!=0 && b!=7 || a==7 && b==6)
		    		casas[a][b].addActionListener(this);
		    }
		}
		
		l.insets = new Insets(5,0,0,0);
		l.gridx = 0;
		l.gridy = 2;
		add(informacao, l);
		
		l.anchor = GridBagConstraints.CENTER;
		l.gridy = 3;
		add(panelTabuleiro, l);
		
		l.anchor = GridBagConstraints.EAST;
		l.gridy = 4;
		add(panelButton, l);
		
		setSize(530, 455);
		setLocationRelativeTo(null);
		panelTabuleiro.setBorder(BorderFactory.createEtchedBorder(1));
		panelTabuleiro.revalidate();
		revalidate();
	}
	private void tabuleirosCasasEspeciais(ActionEvent e){
		for (int x = 0; x < 8; x++) {
		    for (int y = 0; y < 8; y++){
		    	if(qnt36.isSelected() == true){
		    		if(x==0 || x==1 && y==0 || x==2 || x==3 && y==7 || x==4 || x==5 && y==0 || x==6 || x==7 && y==7){
			    		if(qntCasas<=5){
			    			if(e.getSource() == casas[x][y]){
			    				if(qntCasas==1){
			    					casaAcao[0] = casas[x][y].getText();
			    					casaAcao[1] = (String) JOptionPane.showInputDialog(null, "1� casa escolhida. Escolha a a��o que ter� a casa de n�mero "+casaAcao[0]+":", "Casas Especiais", -1, null, acoes, acoes[0]);
			    					if(casaAcao[1] != null){
			    						qntCasas++;
			    						casas[x][y].setEnabled(false);
			    					}
			    				} else if(qntCasas==2){
			    					casaAcao[2] = casas[x][y].getText();
			    					casaAcao[3] = (String) JOptionPane.showInputDialog(null, "2� casa escolhida. Escolha a a��o que ter� a casa de n�mero "+casaAcao[2]+":", "Casas Especiais", -1, null, acoes, acoes[0]);
			    					if(casaAcao[3] != null){
			    						qntCasas++;
			    						casas[x][y].setEnabled(false);
			    					}
			    				} else if(qntCasas==3){
			    					casaAcao[4] = casas[x][y].getText();
			    					casaAcao[5] = (String) JOptionPane.showInputDialog(null, "3� casa escolhida. Escolha a a��o que ter� a casa de n�mero "+casaAcao[4]+":", "Casas Especiais", -1, null, acoes, acoes[0]);
			    					if(casaAcao[5] != null){
			    						qntCasas++;
			    						casas[x][y].setEnabled(false);
			    					}
			    				} else if(qntCasas==4){
			    					casaAcao[6] = casas[x][y].getText();
			    					casaAcao[7] = (String) JOptionPane.showInputDialog(null, "4� casa escolhida. Escolha a a��o que ter� a casa de n�mero "+casaAcao[6]+":", "Casas Especiais", -1, null, acoes, acoes[0]);
			    					if(casaAcao[7] != null){
			    						qntCasas++;
			    						casas[x][y].setEnabled(false);
			    					}
			    				} else if(qntCasas==5){
			    					casaAcao[8] = casas[x][y].getText();
			    					casaAcao[9] = (String) JOptionPane.showInputDialog(null, "5� casa escolhida. Escolha a a��o que ter� a casa de n�mero "+casaAcao[8]+":", "Casas Especiais", -1, null, acoes, acoes[0]);
			    					if(casaAcao[9] != null){
			    						qntCasas++;
			    						casas[x][y].setEnabled(false);
			    					}
			    				}
			    			}
			    			} else {
			    				for (int l=0; l<8; l++) {
			    				    for (int c=0; c<8; c++){
			    				    	if(l==0 || l==1 && c==0 || l==2 || l==3 && c==7 || l==4 || l==5 && c==0 || l==6 || l==7 && c==7){
					    		    		casas[l][c].setEnabled(false);
					    					register.setEnabled(true);
			    				    	}
			    				    }
			    				}
				    		panelTabuleiro.revalidate();
				    		revalidate();
			    		} 
			    	}
		    	} else if(qnt32.isSelected() == true){
		    		if(x==0 && y!=0 || x==1 && y==1 || x==2 && y!=0 || x==3 && y==7 || x==4 && y!=0 || x==5 && y==1 || x==6 && y!=0 || x==7 && y==7){
			    		if(qntCasas<=5){
			    			if(e.getSource() == casas[x][y]){
			    				if(qntCasas==1){
			    					casaAcao[0] = casas[x][y].getText();
			    					casaAcao[1] = (String) JOptionPane.showInputDialog(null, "1� casa escolhida. Escolha a a��o que ter� a casa de n�mero "+casaAcao[0]+":", "Casas Especiais", -1, null, acoes, acoes[0]);
			    					if(casaAcao[1] != null){
			    						qntCasas++;
			    						casas[x][y].setEnabled(false);
			    					}
			    				} else if(qntCasas==2){
			    					casaAcao[2] = casas[x][y].getText();
			    					casaAcao[3] = (String) JOptionPane.showInputDialog(null, "2� casa escolhida. Escolha a a��o que ter� a casa de n�mero "+casaAcao[2]+":", "Casas Especiais", -1, null, acoes, acoes[0]);
			    					if(casaAcao[3] != null){
			    						qntCasas++;
			    						casas[x][y].setEnabled(false);
			    					}
			    				} else if(qntCasas==3){
			    					casaAcao[4] = casas[x][y].getText();
			    					casaAcao[5] = (String) JOptionPane.showInputDialog(null, "3� casa escolhida. Escolha a a��o que ter� a casa de n�mero "+casaAcao[4]+":", "Casas Especiais", -1, null, acoes, acoes[0]);
			    					if(casaAcao[5] != null){
			    						qntCasas++;
			    						casas[x][y].setEnabled(false);
			    					}
			    				} else if(qntCasas==4){
			    					casaAcao[6] = casas[x][y].getText();
			    					casaAcao[7] = (String) JOptionPane.showInputDialog(null, "4� casa escolhida. Escolha a a��o que ter� a casa de n�mero "+casaAcao[6]+":", "Casas Especiais", -1, null, acoes, acoes[0]);
			    					if(casaAcao[7] != null){
			    						qntCasas++;
			    						casas[x][y].setEnabled(false);
			    					}
			    				} else if(qntCasas==5){
			    					casaAcao[8] = casas[x][y].getText();
			    					casaAcao[9] = (String) JOptionPane.showInputDialog(null, "5� casa escolhida. Escolha a a��o que ter� a casa de n�mero "+casaAcao[8]+":", "Casas Especiais", -1, null, acoes, acoes[0]);
			    					if(casaAcao[9] != null){
			    						qntCasas++;
			    						casas[x][y].setEnabled(false);
			    					}
			    				}
			    			}
			    			} else {
			    				for (int l=0; l<8; l++) {
			    				    for (int c=0; c<8; c++){
			    				    	if(l==0 && c!=0 || l==1 && c==1 || l==2 && c!=0 || l==3 && c==7 || l==4 && c!=0 || l==5 && c==1 || l==6 && c!=0 || l==7 && c==7){
					    		    		casas[l][c].setEnabled(false);
					    		    		register.setEnabled(true);
			    				    	}
			    				    }
			    				}
				    		panelTabuleiro.revalidate();
				    		revalidate();
			    		} 
			    	}
		    	} else if(qnt28.isSelected() == true){
		    		if(x==0 && y!=0 && y!=7 || x==1 && y==1 || x==2 && y!=0 && y!=7 || x==3 && y==6 || x==4 && y!=0 && y!=7 || x==5 && y==1 || x==6 && y!=0 && y!=7 || x==7 && y==6){
			    		if(qntCasas<=5){
			    			if(e.getSource() == casas[x][y]){
			    				if(qntCasas==1){
			    					casaAcao[0] = casas[x][y].getText();
			    					casaAcao[1] = (String) JOptionPane.showInputDialog(null, "1� casa escolhida. Escolha a a��o que ter� a casa de n�mero "+casaAcao[0]+":", "Casas Especiais", -1, null, acoes, acoes[0]);
			    					if(casaAcao[1] != null){
			    						qntCasas++;
			    						casas[x][y].setEnabled(false);
			    					}
			    				} else if(qntCasas==2){
			    					casaAcao[2] = casas[x][y].getText();
			    					casaAcao[3] = (String) JOptionPane.showInputDialog(null, "2� casa escolhida. Escolha a a��o que ter� a casa de n�mero "+casaAcao[2]+":", "Casas Especiais", -1, null, acoes, acoes[0]);
			    					if(casaAcao[3] != null){
			    						qntCasas++;
			    						casas[x][y].setEnabled(false);
			    					}
			    				} else if(qntCasas==3){
			    					casaAcao[4] = casas[x][y].getText();
			    					casaAcao[5] = (String) JOptionPane.showInputDialog(null, "3� casa escolhida. Escolha a a��o que ter� a casa de n�mero "+casaAcao[4]+":", "Casas Especiais", -1, null, acoes, acoes[0]);
			    					if(casaAcao[5] != null){
			    						qntCasas++;
			    						casas[x][y].setEnabled(false);
			    					}
			    				} else if(qntCasas==4){
			    					casaAcao[6] = casas[x][y].getText();
			    					casaAcao[7] = (String) JOptionPane.showInputDialog(null, "4� casa escolhida. Escolha a a��o que ter� a casa de n�mero "+casaAcao[6]+":", "Casas Especiais", -1, null, acoes, acoes[0]);
			    					if(casaAcao[7] != null){
			    						qntCasas++;
			    						casas[x][y].setEnabled(false);
			    					}
			    				} else if(qntCasas==5){
			    					casaAcao[8] = casas[x][y].getText();
			    					casaAcao[9] = (String) JOptionPane.showInputDialog(null, "5� casa escolhida. Escolha a a��o que ter� a casa de n�mero "+casaAcao[8]+":", "Casas Especiais", -1, null, acoes, acoes[0]);
			    					if(casaAcao[9] != null){
			    						qntCasas++;
			    						casas[x][y].setEnabled(false);
			    					}
			    				}
			    			}
			    			} else {
			    				for (int l=0; l<8; l++) {
			    				    for (int c=0; c<8; c++){
			    				    	if(l==0 && c!=0 && c!=7 || l==1 && c==1 || l==2 && c!=0 && c!=7 || l==3 && c==6 || l==4 && c!=0 && c!=7 || l==5 && c==1 || l==6 && c!=0 && c!=7 || l==7 && c==6){
					    		    		casas[l][c].setEnabled(false);
					    		    		register.setEnabled(true);
			    				    	}
			    				    }
			    				}
				    		panelTabuleiro.revalidate();
				    		revalidate();
			    		} 
			    	}
    		    }
    		} // for 2
		} // for 1
		
		
	}
	private String[] casasEspeciais(String[] casaAcao){
		
		Random a = new Random();
		int casa = 0, indiceAcao, c=0;
		String[] acoes = {"Pular uma casa", "Ficar sem jogar uma rodada", "Joque outra vez"};
		int[] casaJaEscolhida = new int [5];
		boolean flag = false;
		
		if(casaAcao[0] == null){
				for(int x=0; x<casaAcao.length; x++){
					if(x==0 || x==2 || x==4 || x==6 || x==8){
						
						if(qnt36.isSelected() == true)
							casa = a.nextInt(35)+1;
						if(qnt32.isSelected() == true)
							casa = a.nextInt(31)+1;
						if(qnt28.isSelected() == true)
							casa = a.nextInt(27)+1;
						
						while(flag == false){
							if(casa == casaJaEscolhida[0] || casa == casaJaEscolhida[1] || casa == casaJaEscolhida[2] || casa == casaJaEscolhida[3] || casa == casaJaEscolhida[4]){
								if(qnt36.isSelected() == true)
									casa = a.nextInt(35)+1;
								if(qnt32.isSelected() == true)
									casa = a.nextInt(31)+1;
								if(qnt28.isSelected() == true)
									casa = a.nextInt(27)+1;
							} else {
								casaJaEscolhida[c] = casa;
								c++;
								if(c == 5)
									c = 0;
								flag = true;
							}
						}
						flag = false;
						
						casaAcao[x] = ""+casa;
				}else if(x==1 || x==3 || x==5 || x==7 || x==9){
						indiceAcao = a.nextInt(3);
						casaAcao[x] = acoes[indiceAcao];
						flag = false;
					}
				}
		}
		
		return casaAcao;
	}
}
