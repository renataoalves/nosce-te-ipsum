package br.novaroma.projetojogo.view;

import java.awt.*;
import java.awt.event.*;
import java.io.IOException;

import javax.swing.*;

import br.novaroma.projetojogo.controller.ControllerJogar;

public class GuiTabuleiro extends JFrame implements ActionListener{
	private static final long serialVersionUID = 1L;
	
	private JMenu menuAjuda = new JMenu("Ajuda");
	private static JMenuItem jogo = new JMenuItem("Jogo");
	private JMenuBar menuBar = new JMenuBar();
	public static JButton jogarDado0 = new JButton("Jogar Dado"),
								  jogarDado1 = new JButton("Jogar Dado"),
								  jogarDado2 = new JButton("Jogar Dado"),
								  jogarDado3 = new JButton("Jogar Dado"),
								  jogarDado4 = new JButton("Jogar Dado"),
								  back = new JButton("Voltar ao Menu");
	public JPanel panelTabuleiro, panelJogarDado = new JPanel(new GridBagLayout()),
					   panelImagem = new Background("/imagens/tabuleiro.png"),
					   panelCentral = new JPanel(new GridBagLayout()),
					   panelButton = new JPanel(new FlowLayout()),
					   panelTimer = new JPanel(new FlowLayout());
	private JLabel casas[][] = new JLabel[8][8], ok0, ok1, ok2, ok3, ok4, tempo;
	public static Timer timer;
	public static boolean a = true;
	public static int outro = 0, i = 60;
	private String qntCasas, corPeao[], nomePeao[];
	private int qntPeoes;
	private GridBagConstraints l = new GridBagConstraints(), b = new GridBagConstraints(), c = new GridBagConstraints();
	
	public GuiTabuleiro(int qntPeoes, String qntCasas, String backG, String[] corPeao, String[] nomePeao){
		super("Nosce Te Ipsum");
		
		setIconImage((new ImageIcon(getClass().getResource("/imagens/icone.png"))).getImage());
		
		this.qntPeoes = qntPeoes;
		this.qntCasas = qntCasas;
		this.corPeao = corPeao;
		this.nomePeao = nomePeao;
		
		menuAjuda.add(jogo);
		jogo.addActionListener(this);
		menuBar.add(menuAjuda);
		setJMenuBar(menuBar);
		
		jogarDado0.setToolTipText("Alt + 1");
		jogarDado0.setMnemonic(KeyEvent.VK_1);
		
		jogarDado1.setToolTipText("Alt + 2");
		jogarDado1.setMnemonic(KeyEvent.VK_2);
		
		jogarDado2.setToolTipText("Alt + 3");
		jogarDado2.setMnemonic(KeyEvent.VK_3);
		
		jogarDado3.setToolTipText("Alt + 4");
		jogarDado3.setMnemonic(KeyEvent.VK_4);
		
		jogarDado4.setToolTipText("Alt + 5");
		jogarDado4.setMnemonic(KeyEvent.VK_5);
		
		background(backG); // imagem do background escolhido pelo usu�rio
		panelTabuleiro.setLayout(new GridBagLayout());
		panelImagem.setLayout(new GridBagLayout());
		
		i = 60;
		timer = new Timer(1000, this);
		timer.start();
		
		l.insets = new Insets(10,0,0,0);
		l.gridx = 0;
		l.gridy = 0;
		panelJogarDado.add(new JLabel(nomePeao[0], new ImageIcon(getClass().getResource(peao(corPeao[0]))), 0), l);
		jogarDado0.setEnabled(true);
		b.gridx = 0;
		b.gridy = 1;
		panelJogarDado.add(jogarDado0, b);
		
		l.gridx = 1;
		l.gridy = 2;
		panelJogarDado.add(new JLabel(nomePeao[1], new ImageIcon(getClass().getResource(peao(corPeao[1]))), 0), l);
		jogarDado1.setEnabled(false);
		b.gridx = 1;
		b.gridy = 4;
		panelJogarDado.add(jogarDado1, b);
		
		if(qntCasas.equals("28 casas")){
			ControllerJogar.y0 = 0;
			ControllerJogar.x0 = 6;
			
			ControllerJogar.y1 = 0;
			ControllerJogar.x1 = 6;
			
			ControllerJogar.y2 = 0;
			ControllerJogar.x2 = 6;
			
			ControllerJogar.y3 = 0;
			ControllerJogar.x3 = 6;
			
			ControllerJogar.y4 = 0;
			ControllerJogar.x4 = 6;
		}

		c.gridx = ControllerJogar.y0;
		c.gridy = ControllerJogar.x0;
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		ok0 = new JLabel(new ImageIcon(getClass().getResource(peao(corPeao[0]))));
		panelTabuleiro.add(ok0, c);
		
		c.gridx = ControllerJogar.y1;
		c.gridy = ControllerJogar.x1;
		c.anchor = GridBagConstraints.FIRST_LINE_END;
		ok1 = new JLabel(new ImageIcon(getClass().getResource(peao(corPeao[1]))));
		panelTabuleiro.add(ok1, c);
		
		outrosComponentes(qntPeoes); // cria os outros bot�es, labels, pe�es conforme a quantidade de pe�es cadastrada no jogo escolhido
		tabuleiro(quantCasas(qntCasas)); // matriz do tabuleiro escolhido pelo usu�rio
		
		panelTabuleiro.setBorder(BorderFactory.createEtchedBorder());
		
		panelTimer.setBorder(BorderFactory.createEtchedBorder());
		panelTimer.setBackground(Color.white);
		
		back.addActionListener(this);
		panelButton.add(back);
		
		jogarDado0.addActionListener(this);
		jogarDado1.addActionListener(this);
		jogarDado2.addActionListener(this);
		jogarDado3.addActionListener(this);
		jogarDado4.addActionListener(this);
		
		tempo = new JLabel("Tempo: 60 segundos");
		tempo.setFont(new Font("Verdana", Font.BOLD, 15));
		panelTimer.add(tempo);
		
		c.gridx = 0;
		c.gridy = 0;
		c.anchor = GridBagConstraints.CENTER;
		if(qntPeoes==5)
			c.anchor = GridBagConstraints.NORTH;
		panelCentral.add(panelJogarDado, c);
		c.anchor = GridBagConstraints.SOUTH;
		panelCentral.add(panelTimer, c);
		c.insets = new Insets(0, 10, 0, 0);
		c.gridx = 1;
		panelCentral.add(panelTabuleiro, c);
		c.gridx = 1;
		c.gridy = 1;
		c.anchor = GridBagConstraints.EAST;
		panelCentral.add(panelButton, c);
		
		panelImagem.add(panelCentral);
		add(panelImagem);
		
		setVisible(true);
		setSize(1087, 680);
        setResizable(false);
        setLocationRelativeTo(null);
	}
	
	private void outrosComponentes(int qntPeoes){
		if(qntPeoes>=3){
			l.gridx = 0;
			l.gridy = 5;
			panelJogarDado.add(new JLabel(nomePeao[2], new ImageIcon(getClass().getResource(peao(corPeao[2]))), 0), l);
			jogarDado2.setEnabled(false);
			
			b.insets = new Insets(0,7,0,0);
			b.gridx = 0;
			b.gridy = 6;
			panelJogarDado.add(jogarDado2, b);
			
			c.gridx = ControllerJogar.y2;
			c.gridy = ControllerJogar.x2;
			if(qntPeoes>=4)
				c.anchor = GridBagConstraints.LAST_LINE_END;
			else
				c.anchor = GridBagConstraints.CENTER;
			ok2 = new JLabel(new ImageIcon(getClass().getResource(peao(corPeao[2]))));
			panelTabuleiro.add(ok2, c);
		}
		
		if(qntPeoes>=4){
			l.gridx = 1;
			l.gridy = 7;
			panelJogarDado.add(new JLabel(nomePeao[3], new ImageIcon(getClass().getResource(peao(corPeao[3]))), 0), l);
			jogarDado3.setEnabled(false);
			
			b.gridx = 1;
			b.gridy = 8;
			panelJogarDado.add(jogarDado3, b);
			
			c.gridx = ControllerJogar.y3;
			c.gridy = ControllerJogar.x3;
			c.anchor = GridBagConstraints.LAST_LINE_START;
			ok3 = new JLabel(new ImageIcon(getClass().getResource(peao(corPeao[3]))));
			panelTabuleiro.add(ok3, c);
		}
		
		if(qntPeoes==5){
			l.gridx = 0;
			l.gridy = 9;
			panelJogarDado.add(new JLabel(nomePeao[4], new ImageIcon(getClass().getResource(peao(corPeao[4]))), 0), l);
			jogarDado4.setEnabled(false);
			
			b.gridx = 0;
			b.gridy = 10;
			panelJogarDado.add(jogarDado4, b);
			
			c.gridx = ControllerJogar.y4;
			c.gridy = ControllerJogar.x4;
			c.anchor = GridBagConstraints.CENTER;
			ok4 = new JLabel(new ImageIcon(getClass().getResource(peao(corPeao[4]))));
			panelTabuleiro.add(ok4, c);
		}
		
	}
	private int quantCasas(String qntCasas){
		if(qntCasas.equals("36 casas"))
			return 36;
		else if(qntCasas.equals("32 casas"))
			return 32;
		else
			return 28;
	}
	private void tabuleiro(int qntCasas){
		c.insets = new Insets(0,1,0,0);
		
		if(qntCasas == 36){
			
			for (int x = 0; x < 8; x++) {
			    for (int y = 0; y < 8; y++) {
			        if(y!=0 && x==1 || y!=7 && x==3 || y!=0 && x==5 || y!=6 && y!=7 && x==7){
			        	c.gridx = x;
			        	c.gridy = y;
			        	casas[x][y] = new JLabel(" ");
			        	panelTabuleiro.add(casas[x][y], c);
			        } else if(x==0 && y==7){
			        	c.gridx = x;
			        	c.gridy = y;
			        	casas[x][y] = new JLabel(new ImageIcon(getClass().getResource("/imagens/partida.png")));
			        	panelTabuleiro.add(casas[x][y], c);
			        } else if(x==7 && y==6){
			        	c.gridx = x;
			        	c.gridy = y;
			        	casas[x][y] = new JLabel(new ImageIcon(getClass().getResource("/imagens/chegada.png")));
			        	panelTabuleiro.add(casas[x][y], c);
			        } else {
		        		c.gridx = x;
		        		c.gridy = y;
		        		casas[x][y] = new JLabel(new ImageIcon(getClass().getResource("/imagens/casas.png")));
		        		panelTabuleiro.add(casas[x][y], c);
			        }
			    }
			}
			
		} else if(qntCasas == 32){
			
			for (int x = 0; x < 8; x++) {
			    for (int y = 0; y < 8; y++) {
			        if(y==0 || y!=1 && x==1 || y!=7 && x==3 || y!=1 && x==5 || y!=6 && y!=7 && x==7){
			        	c.gridx = x;
			        	c.gridy = y;
			        	casas[x][y] = new JLabel(" ");
			        	panelTabuleiro.add(casas[x][y], c);
			        } else if(x==0 && y==7){
			        	c.gridx = x;
			        	c.gridy = y;
			        	casas[x][y] = new JLabel(new ImageIcon(getClass().getResource("/imagens/partida.png")));
			        	panelTabuleiro.add(casas[x][y], c);
			        } else if(x==7 && y==6){
			        	c.gridx = x;
			        	c.gridy = y;
			        	casas[x][y] = new JLabel(new ImageIcon(getClass().getResource("/imagens/chegada.png")));
			        	panelTabuleiro.add(casas[x][y], c);
			        } else {
		        		c.gridx = x;
		        		c.gridy = y;
		        		casas[x][y] = new JLabel(new ImageIcon(getClass().getResource("/imagens/casas.png")));
		        		panelTabuleiro.add(casas[x][y], c);
			        }
			    }
			}
			
		} else if (qntCasas == 28){
			
			for (int x = 0; x < 8; x++) {
			    for (int y = 0; y < 8; y++) {
			        if(y==0 || y==7 || y!=1 && x==1 || y!=6 && x==3 || y!=1 && x==5 || y!=5 && y!=6 && x==7){
			        	c.gridx = x;
			        	c.gridy = y;
			        	casas[x][y] = new JLabel(" ");
			        	panelTabuleiro.add(casas[x][y], c);
			        } else if(x==0 && y==6){
			        	c.gridx = x;
			        	c.gridy = y;
			        	casas[x][y] = new JLabel(new ImageIcon(getClass().getResource("/imagens/partida.png")));
			        	panelTabuleiro.add(casas[x][y], c);
			        } else if(x==7 && y==5){
			        	c.gridx = x;
			        	c.gridy = y;
			        	casas[x][y] = new JLabel(new ImageIcon(getClass().getResource("/imagens/chegada.png")));
			        	panelTabuleiro.add(casas[x][y], c);
			        } else {
		        		c.gridx = x;
		        		c.gridy = y;
		        		casas[x][y] = new JLabel(new ImageIcon(getClass().getResource("/imagens/casas.png")));
		        		panelTabuleiro.add(casas[x][y], c);
			        }
			    }
			}
			
		}
	}
	private void background(String backG){
		if(backG.equals("Azul Claro")){
			panelTabuleiro = new JPanel();
			panelTabuleiro.setBackground(Color.CYAN);
		} else
			try {
				panelTabuleiro = new JImagePanel(backG);
			} catch (IOException e) {
				e.getStackTrace();
			}
	}
	private String peao(String peao){
		if(peao.equals("Verde"))
			return "/peoes/peaoVerde.png";
		else if(peao.equals("Amarelo"))	
			return "/peoes/peaoAmarelo.png";
		else if(peao.equals("Azul"))
			return "/peoes/peaoAzul.png";
		else if(peao.equals("Branco"))
			return "/peoes/peaoBranco.png";
		else if(peao.equals("Roxo"))
			return "/peoes/peaoRoxo.png";
		return null;
	}
	protected static void zerarPosicaoPeao(){
		/*Controller.x0 = 7;
		Controller.y0 = 0;
		
		Controller.x1 = 7;
		Controller.y1 = 0;
		
		Controller.x2 = 7;
		Controller.y2 = 0;
		
		Controller.x3 = 7;
		Controller.y3 = 0;
		
		Controller.x4 = 7;
		Controller.y4 = 0;*/
		
		jogarDado0.setEnabled(true);
		jogarDado1.setEnabled(false);
		jogarDado2.setEnabled(false);
		jogarDado3.setEnabled(false);
		jogarDado4.setEnabled(false);
	}
	private void atualizarSegundos(int n){
		if(10<n){
			tempo.setText("Tempo: "+(n-1)+" segundos");
			tempo.setForeground(Color.BLACK);
		}else{
			tempo.setText("Tempo: "+(n-1)+" segundos");
			tempo.setForeground(Color.RED);
		}
	  }
	public void actionPerformed(ActionEvent e){

		if(e.getSource() == back){
			PrincipalMain.menu.setVisible(true);
			dispose();
			zerarPosicaoPeao();
			i = 60;
			timer.stop();
			a = true;
			outro = 0;
			ControllerJogar.jogarDnv0 = false;
			ControllerJogar.jogarDnv1 = false;
			ControllerJogar.jogarDnv2 = false;
			ControllerJogar.jogarDnv3 = false;
			ControllerJogar.jogarDnv4 = false;
			ControllerJogar.ficarSemJogar0 = false;
			ControllerJogar.ficarSemJogar1 = false;
			ControllerJogar.ficarSemJogar2 = false;
			ControllerJogar.ficarSemJogar3 = false;
			ControllerJogar.ficarSemJogar4 = false;
		}
		
		if(e.getSource() == jogo)
			new GuiManualJogo();
		
		if(e.getSource() == timer){
			if(i > 0 && i <= 60){
				atualizarSegundos(i);
				i--;
			} else {
				timer.stop();
			
				if(qntPeoes == 2){
					if(a==true){
						i = 60;
						a = false;
						GuiTabuleiro.jogarDado0.setEnabled(false);
						JOptionPane.showMessageDialog(null, "Tempo esgotado "+nomePeao[0]+"!", "Tempo Esgotado", -1);
						GuiTabuleiro.jogarDado1.setEnabled(true);
						timer.start();
					} else {
						i = 60;
						a = true;
						GuiTabuleiro.jogarDado0.setEnabled(true);
						JOptionPane.showMessageDialog(null, "Tempo esgotado "+nomePeao[1]+"!", "Tempo Esgotado", -1);
						GuiTabuleiro.jogarDado1.setEnabled(false);
						timer.start();
					}
				}// 2 jogadores
				else if(qntPeoes == 3){
					if(outro == 0){
						if(a==true){
							i = 60;
							a = false;
							GuiTabuleiro.jogarDado0.setEnabled(false);
							JOptionPane.showMessageDialog(null, "Tempo esgotado "+nomePeao[0]+"!", "Tempo Esgotado", -1);
							GuiTabuleiro.jogarDado1.setEnabled(true);
							timer.start();
						} else {
							i = 60;
							a = true;
							outro = 1;
							GuiTabuleiro.jogarDado1.setEnabled(false);
							JOptionPane.showMessageDialog(null, "Tempo esgotado "+nomePeao[1]+"!", "Tempo Esgotado", -1);
							GuiTabuleiro.jogarDado2.setEnabled(true);
							timer.start();
						}
					} else {
						if(a==true){
							i = 60;
							outro = 0;
							GuiTabuleiro.jogarDado2.setEnabled(false);
							JOptionPane.showMessageDialog(null, "Tempo esgotado "+nomePeao[2]+"!", "Tempo Esgotado", -1);
							GuiTabuleiro.jogarDado0.setEnabled(true);
							timer.start();
						}
					}
				}// 3 jogadores
				else if(qntPeoes == 4){
					if(outro == 0){
						if(a==true){
							i = 60;
							a = false;
							GuiTabuleiro.jogarDado0.setEnabled(false);
							JOptionPane.showMessageDialog(null, "Tempo esgotado "+nomePeao[0]+"!", "Tempo Esgotado", -1);
							GuiTabuleiro.jogarDado1.setEnabled(true);
							timer.start();
						} else {
							i = 60;
							a = true;
							outro = 1;
							GuiTabuleiro.jogarDado1.setEnabled(false);
							JOptionPane.showMessageDialog(null, "Tempo esgotado "+nomePeao[1]+"!", "Tempo Esgotado", -1);
							GuiTabuleiro.jogarDado2.setEnabled(true);
							timer.start();
						}
					} else {
						if(a==true){
							i = 60;
							a = false;
							GuiTabuleiro.jogarDado2.setEnabled(false);
							JOptionPane.showMessageDialog(null, "Tempo esgotado "+nomePeao[2]+"!", "Tempo Esgotado", -1);
							GuiTabuleiro.jogarDado3.setEnabled(true);
							timer.start();
						} else {
							i = 60;
							a = true;
							outro = 0;
							GuiTabuleiro.jogarDado3.setEnabled(false);
							JOptionPane.showMessageDialog(null, "Tempo esgotado "+nomePeao[3]+"!", "Tempo Esgotado", -1);
							GuiTabuleiro.jogarDado0.setEnabled(true);
							timer.start();
						}
					}
				}// 4 jogadores
				else { // 5 jogadores
					if(outro == 0){
						if(a==true){
							i = 60;
							a = false;
							GuiTabuleiro.jogarDado0.setEnabled(false);
							JOptionPane.showMessageDialog(null, "Tempo esgotado "+nomePeao[0]+"!", "Tempo Esgotado", -1);
							GuiTabuleiro.jogarDado1.setEnabled(true);
							timer.start();
						} else {
							i = 60;
							a = true;
							outro = 1;
							GuiTabuleiro.jogarDado1.setEnabled(false);
							JOptionPane.showMessageDialog(null, "Tempo esgotado "+nomePeao[1]+"!", "Tempo Esgotado", -1);
							GuiTabuleiro.jogarDado2.setEnabled(true);
							timer.start();
						}
					} else if(outro==1){
						if(a==true){
							i = 60;
							a = false;
							GuiTabuleiro.jogarDado2.setEnabled(false);
							JOptionPane.showMessageDialog(null, "Tempo esgotado "+nomePeao[2]+"!", "Tempo Esgotado", -1);
							GuiTabuleiro.jogarDado3.setEnabled(true);
							timer.start();
						} else {
							i = 60;
							a = true;
							outro = 2;
							GuiTabuleiro.jogarDado3.setEnabled(false);
							JOptionPane.showMessageDialog(null, "Tempo esgotado "+nomePeao[3]+"!", "Tempo Esgotado", -1);
							GuiTabuleiro.jogarDado4.setEnabled(true);
							timer.start();
						}
					} else if(outro==2){
						if(a==true){
							i = 60;
							a = true;
							outro = 0;
							GuiTabuleiro.jogarDado4.setEnabled(false);
							JOptionPane.showMessageDialog(null, "Tempo esgotado "+nomePeao[4]+"!", "Tempo Esgotado", -1);
							GuiTabuleiro.jogarDado0.setEnabled(true);
							timer.start();
						}
					}
				}// 5 jogadores
			}
		}
			
		if(qntPeoes == 2){
			if(e.getSource() == jogarDado0){
				
				timer.stop();
				ControllerJogar.jogar(0, corPeao[0], nomePeao[0]);
				
				if(ControllerJogar.jogarDnv0 == false){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(true);
				} else {
					jogarDado0.setEnabled(true);
					jogarDado1.setEnabled(false);
					ControllerJogar.jogarDnv0 = false;
				}
				
				if(ControllerJogar.ficarSemJogar0 == false && ControllerJogar.ficarSemJogar1 == true){
					jogarDado0.setEnabled(true);
					jogarDado1.setEnabled(false);
					ControllerJogar.ficarSemJogar1 = false;
				} else if(ControllerJogar.ficarSemJogar0 == true && ControllerJogar.ficarSemJogar1 == false){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(true);
				} else if(ControllerJogar.ficarSemJogar0 == true && ControllerJogar.ficarSemJogar1 == true){
					jogarDado0.setEnabled(true);
					jogarDado1.setEnabled(false);
					ControllerJogar.ficarSemJogar0 = false;
					ControllerJogar.ficarSemJogar1 = false;
				}
				
				timer.start();
				panelTabuleiro.removeAll();
				c.gridx = ControllerJogar.y0;
				c.gridy = ControllerJogar.x0;
				c.anchor = GridBagConstraints.WEST;
				panelTabuleiro.add(ok0, c);
				c.gridx = ControllerJogar.y1;
				c.gridy = ControllerJogar.x1;
				c.anchor = GridBagConstraints.EAST;
				panelTabuleiro.add(ok1, c);
				
				tabuleiro(quantCasas(qntCasas)); // matriz do tabuleiro escolhido pelo usu�rio
				panelTabuleiro.revalidate();
			}
			if(e.getSource() == jogarDado1){
				
				timer.stop();
				ControllerJogar.jogar(1, corPeao[1], nomePeao[1]);
				
				if(ControllerJogar.jogarDnv1 == false){
					jogarDado0.setEnabled(true);
					jogarDado1.setEnabled(false);
				} else {
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(true);
					ControllerJogar.jogarDnv1 = false;
				}
				
				if(ControllerJogar.ficarSemJogar0 == true && ControllerJogar.ficarSemJogar1 == false){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(true);
					ControllerJogar.ficarSemJogar0 = false;
				} else if(ControllerJogar.ficarSemJogar0 == false && ControllerJogar.ficarSemJogar1 == true){
					jogarDado0.setEnabled(true);
					jogarDado1.setEnabled(false);
				} else if(ControllerJogar.ficarSemJogar0 == true && ControllerJogar.ficarSemJogar1 == true){
					jogarDado0.setEnabled(true);
					jogarDado1.setEnabled(false);
					ControllerJogar.ficarSemJogar0 = false;
					ControllerJogar.ficarSemJogar1 = false;
				}
				
				timer.start();
				panelTabuleiro.removeAll();
				c.gridx = ControllerJogar.y0;
				c.gridy = ControllerJogar.x0;
				c.anchor = GridBagConstraints.WEST;
				panelTabuleiro.add(ok0, c);
				c.gridx = ControllerJogar.y1;
				c.gridy = ControllerJogar.x1;
				c.anchor = GridBagConstraints.EAST;
				panelTabuleiro.add(ok1, c);
				tabuleiro(quantCasas(qntCasas));
				panelTabuleiro.revalidate();
			}
		}// 2 jogadores

		else if(qntPeoes == 3){
			if(e.getSource() == jogarDado0){
				
				timer.stop();
				ControllerJogar.jogar(0, corPeao[0], nomePeao[0]);
				
				if(ControllerJogar.jogarDnv0 == false){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(true);
				} else {
					jogarDado0.setEnabled(true);
					jogarDado1.setEnabled(false);
					ControllerJogar.jogarDnv0 = false;
				}
				
				if(ControllerJogar.ficarSemJogar1 == true && ControllerJogar.ficarSemJogar2 == false){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(true);
					ControllerJogar.ficarSemJogar1 = false;
				} else if(ControllerJogar.ficarSemJogar1 == false && ControllerJogar.ficarSemJogar2 == true){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(true);
					jogarDado2.setEnabled(false);
				}  else if(ControllerJogar.ficarSemJogar1 == true && ControllerJogar.ficarSemJogar2 == true){
					jogarDado0.setEnabled(true);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					ControllerJogar.ficarSemJogar1 = false;
					ControllerJogar.ficarSemJogar2 = false;
				}
				
				timer.start();
				panelTabuleiro.removeAll();
				c.gridx = ControllerJogar.y0;
				c.gridy = ControllerJogar.x0;
				c.anchor = GridBagConstraints.WEST;
				panelTabuleiro.add(ok0, c);
				c.gridx = ControllerJogar.y1;
				c.gridy = ControllerJogar.x1;
				c.anchor = GridBagConstraints.EAST;
				panelTabuleiro.add(ok1, c);
				c.gridx = ControllerJogar.y2;
				c.gridy = ControllerJogar.x2;
				c.anchor = GridBagConstraints.SOUTH;
				panelTabuleiro.add(ok2, c);
				
				tabuleiro(quantCasas(qntCasas));
				panelTabuleiro.revalidate();
			}
			if(e.getSource() == jogarDado1){
				
				timer.stop();
				ControllerJogar.jogar(1, corPeao[1], nomePeao[1]);
				
				if(ControllerJogar.jogarDnv1 == false){
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(true);
				} else {
					jogarDado1.setEnabled(true);
					jogarDado2.setEnabled(false);
					ControllerJogar.jogarDnv1 = false;
				}
				
				if(ControllerJogar.ficarSemJogar0 == true && ControllerJogar.ficarSemJogar2 == false){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(true);
				} else if(ControllerJogar.ficarSemJogar0 == false && ControllerJogar.ficarSemJogar2 == true){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(true);
					ControllerJogar.ficarSemJogar2 = false;
				} else if(ControllerJogar.ficarSemJogar0 == true && ControllerJogar.ficarSemJogar2 == true){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(true);
					jogarDado2.setEnabled(false);
					ControllerJogar.ficarSemJogar0 = false;
				}
				
				timer.start();
				panelTabuleiro.removeAll();
				c.gridx = ControllerJogar.y0;
				c.gridy = ControllerJogar.x0;
				c.anchor = GridBagConstraints.WEST;
				panelTabuleiro.add(ok0, c);
				c.gridx = ControllerJogar.y1;
				c.gridy = ControllerJogar.x1;
				c.anchor = GridBagConstraints.EAST;
				panelTabuleiro.add(ok1, c);
				c.gridx = ControllerJogar.y2;
				c.gridy = ControllerJogar.x2;
				c.anchor = GridBagConstraints.SOUTH;
				panelTabuleiro.add(ok2, c);
				
				tabuleiro(quantCasas(qntCasas));
				panelTabuleiro.revalidate();
			}
			if(e.getSource() == jogarDado2){
				
				timer.stop();
				ControllerJogar.jogar(2, corPeao[2], nomePeao[2]);
				
				if(ControllerJogar.jogarDnv2 == false){
					jogarDado2.setEnabled(false);
					jogarDado0.setEnabled(true);
				} else {
					jogarDado2.setEnabled(true);
					jogarDado0.setEnabled(false);
					ControllerJogar.jogarDnv2 = false;
				}
				
				if(ControllerJogar.ficarSemJogar0 == true && ControllerJogar.ficarSemJogar1 == false){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(true);
					jogarDado2.setEnabled(false);
					ControllerJogar.ficarSemJogar0 = false;
				} else if(ControllerJogar.ficarSemJogar0 == false && ControllerJogar.ficarSemJogar1 == true){
					jogarDado0.setEnabled(true);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
				} else if(ControllerJogar.ficarSemJogar0 == true && ControllerJogar.ficarSemJogar1 == true){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(true);
					ControllerJogar.ficarSemJogar0 = false;
					ControllerJogar.ficarSemJogar1 = false;
				}
				
				timer.start();
				panelTabuleiro.removeAll();
				c.gridx = ControllerJogar.y0;
				c.gridy = ControllerJogar.x0;
				c.anchor = GridBagConstraints.WEST;
				panelTabuleiro.add(ok0, c);
				c.gridx = ControllerJogar.y1;
				c.gridy = ControllerJogar.x1;
				c.anchor = GridBagConstraints.EAST;
				panelTabuleiro.add(ok1, c);
				c.gridx = ControllerJogar.y2;
				c.gridy = ControllerJogar.x2;
				c.anchor = GridBagConstraints.SOUTH;
				panelTabuleiro.add(ok2, c);
				
				tabuleiro(quantCasas(qntCasas));
				panelTabuleiro.revalidate();
			}
		}// 3 jogadores

		else if(qntPeoes == 4){
			if(e.getSource() == jogarDado0){
				
				timer.stop();
				ControllerJogar.jogar(0, corPeao[0], nomePeao[0]);
				
				if(ControllerJogar.jogarDnv0 == false){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(true);
				} else {
					jogarDado0.setEnabled(true);
					jogarDado1.setEnabled(false);
					ControllerJogar.jogarDnv0 = false;
				}
				
				if(ControllerJogar.ficarSemJogar1 == true && ControllerJogar.ficarSemJogar2 == false && ControllerJogar.ficarSemJogar3 == false){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(true);
					jogarDado3.setEnabled(false);
					ControllerJogar.ficarSemJogar1 = false;
				} else if(ControllerJogar.ficarSemJogar1 == false && ControllerJogar.ficarSemJogar2 == true	&& ControllerJogar.ficarSemJogar3 == false){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(true);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
				} else if(ControllerJogar.ficarSemJogar1 == false && ControllerJogar.ficarSemJogar2 == false	 && ControllerJogar.ficarSemJogar3 == true){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(true);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
				} else if(ControllerJogar.ficarSemJogar1 == true && ControllerJogar.ficarSemJogar2 == true && ControllerJogar.ficarSemJogar3 == false){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(true);
					ControllerJogar.ficarSemJogar1 = false;
				} else if(ControllerJogar.ficarSemJogar1 == true && ControllerJogar.ficarSemJogar2 == false && ControllerJogar.ficarSemJogar3 == true){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(true);
					jogarDado3.setEnabled(false);
					ControllerJogar.ficarSemJogar1 = false;
				} else if(ControllerJogar.ficarSemJogar1 == false && ControllerJogar.ficarSemJogar2 == true && ControllerJogar.ficarSemJogar3 == true){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(true);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
				} else if(ControllerJogar.ficarSemJogar1 == true && ControllerJogar.ficarSemJogar2 == true && ControllerJogar.ficarSemJogar3 == true){
					jogarDado0.setEnabled(true);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
					ControllerJogar.ficarSemJogar1 = false;
					ControllerJogar.ficarSemJogar2 = false;
					ControllerJogar.ficarSemJogar3 = false;
				}
				
				timer.start();
				panelTabuleiro.removeAll();
				c.gridx = ControllerJogar.y0;
				c.gridy = ControllerJogar.x0;
				c.anchor = GridBagConstraints.FIRST_LINE_START;
				panelTabuleiro.add(ok0, c);
				c.gridx = ControllerJogar.y1;
				c.gridy = ControllerJogar.x1;
				c.anchor = GridBagConstraints.FIRST_LINE_END;
				panelTabuleiro.add(ok1, c);
				c.gridx = ControllerJogar.y2;
				c.gridy = ControllerJogar.x2;
				c.anchor = GridBagConstraints.LAST_LINE_START;
				panelTabuleiro.add(ok2, c);
				c.gridx = ControllerJogar.y3;
				c.gridy = ControllerJogar.x3;
				c.anchor = GridBagConstraints.LAST_LINE_END;
				panelTabuleiro.add(ok3, c);
				
				tabuleiro(quantCasas(qntCasas));
				panelTabuleiro.revalidate();
			}
			if(e.getSource() == jogarDado1){
				
				timer.stop();
				ControllerJogar.jogar(1, corPeao[1], nomePeao[1]);
				
				if(ControllerJogar.jogarDnv1 == false){
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(true);
				} else {
					jogarDado1.setEnabled(true);
					jogarDado2.setEnabled(false);
					ControllerJogar.jogarDnv1 = false;
				}
				
				if(ControllerJogar.ficarSemJogar0 == true && ControllerJogar.ficarSemJogar2 == false && ControllerJogar.ficarSemJogar3 == false){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(true);
					jogarDado3.setEnabled(false);
				} else if(ControllerJogar.ficarSemJogar0 == false && ControllerJogar.ficarSemJogar2 == true	&& ControllerJogar.ficarSemJogar3 == false){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(true);
					ControllerJogar.ficarSemJogar2 = false;
				} else if(ControllerJogar.ficarSemJogar0 == false && ControllerJogar.ficarSemJogar2 == false	 && ControllerJogar.ficarSemJogar3 == true){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(true);
					jogarDado3.setEnabled(false);
				} else if(ControllerJogar.ficarSemJogar0 == true && ControllerJogar.ficarSemJogar2 == true && ControllerJogar.ficarSemJogar3 == false){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(true);
					ControllerJogar.ficarSemJogar2 = false;
				} else if(ControllerJogar.ficarSemJogar0 == true && ControllerJogar.ficarSemJogar2 == false && ControllerJogar.ficarSemJogar3 == true){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(true);
					jogarDado3.setEnabled(false);
				} else if(ControllerJogar.ficarSemJogar0 == false && ControllerJogar.ficarSemJogar2 == true && ControllerJogar.ficarSemJogar3 == true){
					jogarDado0.setEnabled(true);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
					ControllerJogar.ficarSemJogar2 = false;
				} else if(ControllerJogar.ficarSemJogar0 == true && ControllerJogar.ficarSemJogar2 == true && ControllerJogar.ficarSemJogar3 == true){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(true);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
					ControllerJogar.ficarSemJogar0 = false;
					ControllerJogar.ficarSemJogar2 = false;
					ControllerJogar.ficarSemJogar3 = false;
				}
				
				timer.start();
				panelTabuleiro.removeAll();
				c.gridx = ControllerJogar.y0;
				c.gridy = ControllerJogar.x0;
				c.anchor = GridBagConstraints.FIRST_LINE_START;
				panelTabuleiro.add(ok0, c);
				c.gridx = ControllerJogar.y1;
				c.gridy = ControllerJogar.x1;
				c.anchor = GridBagConstraints.FIRST_LINE_END;
				panelTabuleiro.add(ok1, c);
				c.gridx = ControllerJogar.y2;
				c.gridy = ControllerJogar.x2;
				c.anchor = GridBagConstraints.LAST_LINE_START;
				panelTabuleiro.add(ok2, c);
				c.gridx = ControllerJogar.y3;
				c.gridy = ControllerJogar.x3;
				c.anchor = GridBagConstraints.LAST_LINE_END;
				panelTabuleiro.add(ok3, c);
				
				tabuleiro(quantCasas(qntCasas));
				panelTabuleiro.revalidate();
			}
			if(e.getSource() == jogarDado2){
				
				timer.stop();
				ControllerJogar.jogar(2, corPeao[2], nomePeao[2]);
				
				if(ControllerJogar.jogarDnv2 == false){
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(true);
				} else {
					jogarDado2.setEnabled(true);
					jogarDado3.setEnabled(false);
					ControllerJogar.jogarDnv2 = false;
				}
				
				if(ControllerJogar.ficarSemJogar0 == true && ControllerJogar.ficarSemJogar1 == false && ControllerJogar.ficarSemJogar3 == false){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(true);
				} else if(ControllerJogar.ficarSemJogar0 == false && ControllerJogar.ficarSemJogar1 == true	&& ControllerJogar.ficarSemJogar3 == false){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(true);
				} else if(ControllerJogar.ficarSemJogar0 == false && ControllerJogar.ficarSemJogar1 == false	 && ControllerJogar.ficarSemJogar3 == true){
					jogarDado0.setEnabled(true);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
					ControllerJogar.ficarSemJogar3 = false;
				} else if(ControllerJogar.ficarSemJogar0 == true && ControllerJogar.ficarSemJogar1 == true && ControllerJogar.ficarSemJogar3 == false){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(true);
				} else if(ControllerJogar.ficarSemJogar0 == true && ControllerJogar.ficarSemJogar1 == false && ControllerJogar.ficarSemJogar3 == true){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(true);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
					ControllerJogar.ficarSemJogar3 = false;
				} else if(ControllerJogar.ficarSemJogar0 == false && ControllerJogar.ficarSemJogar1 == true && ControllerJogar.ficarSemJogar3 == true){
					jogarDado0.setEnabled(true);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
					ControllerJogar.ficarSemJogar3 = false;
				} else if(ControllerJogar.ficarSemJogar0 == true && ControllerJogar.ficarSemJogar1 == true && ControllerJogar.ficarSemJogar3 == true){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(true);
					jogarDado3.setEnabled(false);
					ControllerJogar.ficarSemJogar0 = false;
					ControllerJogar.ficarSemJogar1 = false;
					ControllerJogar.ficarSemJogar3 = false;
				}
				
				timer.start();
				panelTabuleiro.removeAll();
				c.gridx = ControllerJogar.y0;
				c.gridy = ControllerJogar.x0;
				c.anchor = GridBagConstraints.FIRST_LINE_START;
				panelTabuleiro.add(ok0, c);
				c.gridx = ControllerJogar.y1;
				c.gridy = ControllerJogar.x1;
				c.anchor = GridBagConstraints.FIRST_LINE_END;
				panelTabuleiro.add(ok1, c);
				c.gridx = ControllerJogar.y2;
				c.gridy = ControllerJogar.x2;
				c.anchor = GridBagConstraints.LAST_LINE_START;
				panelTabuleiro.add(ok2, c);
				c.gridx = ControllerJogar.y3;
				c.gridy = ControllerJogar.x3;
				c.anchor = GridBagConstraints.LAST_LINE_END;
				panelTabuleiro.add(ok3, c);
				
				tabuleiro(quantCasas(qntCasas));
				panelTabuleiro.revalidate();
			}
			if(e.getSource() == jogarDado3){
				
				timer.stop();
				ControllerJogar.jogar(3, corPeao[3], nomePeao[3]);
				
				if(ControllerJogar.jogarDnv3 == false){
					jogarDado3.setEnabled(false);
					jogarDado0.setEnabled(true);
				} else {
					jogarDado3.setEnabled(true);
					jogarDado0.setEnabled(false);
					ControllerJogar.jogarDnv3 = false;
				}
				
				if(ControllerJogar.ficarSemJogar0 == true && ControllerJogar.ficarSemJogar1 == false && ControllerJogar.ficarSemJogar2 == false){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(true);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
					ControllerJogar.ficarSemJogar0 = false;
				} else if(ControllerJogar.ficarSemJogar0 == false && ControllerJogar.ficarSemJogar1 == true	&& ControllerJogar.ficarSemJogar2 == false){
					jogarDado0.setEnabled(true);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
				} else if(ControllerJogar.ficarSemJogar0 == false && ControllerJogar.ficarSemJogar1 == false	 && ControllerJogar.ficarSemJogar2 == true){
					jogarDado0.setEnabled(true);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
				} else if(ControllerJogar.ficarSemJogar0 == true && ControllerJogar.ficarSemJogar1 == true && ControllerJogar.ficarSemJogar2 == false){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(true);
					jogarDado3.setEnabled(false);
					ControllerJogar.ficarSemJogar0 = false;
				} else if(ControllerJogar.ficarSemJogar0 == true && ControllerJogar.ficarSemJogar1 == false && ControllerJogar.ficarSemJogar2 == true){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(true);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
					ControllerJogar.ficarSemJogar0 = false;
				} else if(ControllerJogar.ficarSemJogar0 == false && ControllerJogar.ficarSemJogar1 == true && ControllerJogar.ficarSemJogar2 == true){
					jogarDado0.setEnabled(true);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
				} else if(ControllerJogar.ficarSemJogar0 == true && ControllerJogar.ficarSemJogar1 == true && ControllerJogar.ficarSemJogar2 == true){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(true);
					ControllerJogar.ficarSemJogar0 = false;
					ControllerJogar.ficarSemJogar1 = false;
					ControllerJogar.ficarSemJogar2 = false;
				}
				
				timer.start();
				panelTabuleiro.removeAll();
				c.gridx = ControllerJogar.y0;
				c.gridy = ControllerJogar.x0;
				c.anchor = GridBagConstraints.FIRST_LINE_START;
				panelTabuleiro.add(ok0, c);
				c.gridx = ControllerJogar.y1;
				c.gridy = ControllerJogar.x1;
				c.anchor = GridBagConstraints.FIRST_LINE_END;
				panelTabuleiro.add(ok1, c);
				c.gridx = ControllerJogar.y2;
				c.gridy = ControllerJogar.x2;
				c.anchor = GridBagConstraints.LAST_LINE_START;
				panelTabuleiro.add(ok2, c);
				c.gridx = ControllerJogar.y3;
				c.gridy = ControllerJogar.x3;
				c.anchor = GridBagConstraints.LAST_LINE_END;
				panelTabuleiro.add(ok3, c);
				
				tabuleiro(quantCasas(qntCasas));
				panelTabuleiro.revalidate();
			}
		} // 4 jogadores

		else if(qntPeoes == 5){ // 5 jogadores
			if(e.getSource() == jogarDado0){
				
				timer.stop();
				ControllerJogar.jogar(0, corPeao[0], nomePeao[0]);
				
				if(ControllerJogar.jogarDnv0 == false){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(true);
				} else {
					jogarDado0.setEnabled(true);
					jogarDado1.setEnabled(false);
					ControllerJogar.jogarDnv0 = false;
				}
				
				if(ControllerJogar.ficarSemJogar1 == true && ControllerJogar.ficarSemJogar2 == false && ControllerJogar.ficarSemJogar3 == false && ControllerJogar.ficarSemJogar4 == false){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(true);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(false);
					ControllerJogar.ficarSemJogar1 = false;
				} else if(ControllerJogar.ficarSemJogar1 == false && ControllerJogar.ficarSemJogar2 == true && ControllerJogar.ficarSemJogar3 == false && ControllerJogar.ficarSemJogar4 == false){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(true);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(false);
				} else if(ControllerJogar.ficarSemJogar1 == false && ControllerJogar.ficarSemJogar2 == false && ControllerJogar.ficarSemJogar3 == true && ControllerJogar.ficarSemJogar4 == false){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(true);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(false);
				} else if(ControllerJogar.ficarSemJogar1 == false && ControllerJogar.ficarSemJogar2 == false && ControllerJogar.ficarSemJogar3 == false && ControllerJogar.ficarSemJogar4 == true){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(true);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(false);
				} else if(ControllerJogar.ficarSemJogar1 == true && ControllerJogar.ficarSemJogar2 == true && ControllerJogar.ficarSemJogar3 == false && ControllerJogar.ficarSemJogar4 == false){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(true);
					jogarDado4.setEnabled(false);
					ControllerJogar.ficarSemJogar1 = false;
					ControllerJogar.ficarSemJogar2 = false;
				} else if(ControllerJogar.ficarSemJogar1 == false && ControllerJogar.ficarSemJogar2 == false && ControllerJogar.ficarSemJogar3 == true && ControllerJogar.ficarSemJogar4 == true){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(true);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(false);
				} else if(ControllerJogar.ficarSemJogar1 == true && ControllerJogar.ficarSemJogar2 == false && ControllerJogar.ficarSemJogar3 == true && ControllerJogar.ficarSemJogar4 == false){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(true);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(false);
					ControllerJogar.ficarSemJogar1 = false;
				} else if(ControllerJogar.ficarSemJogar1 == false && ControllerJogar.ficarSemJogar2 == true && ControllerJogar.ficarSemJogar3 == false && ControllerJogar.ficarSemJogar4 == true){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(true);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(false);
				} else if(ControllerJogar.ficarSemJogar1 == true && ControllerJogar.ficarSemJogar2 == false && ControllerJogar.ficarSemJogar3 == false && ControllerJogar.ficarSemJogar4 == true){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(true);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(false);
					ControllerJogar.ficarSemJogar1 = false;
				} else if(ControllerJogar.ficarSemJogar1 == false && ControllerJogar.ficarSemJogar2 == true && ControllerJogar.ficarSemJogar3 == true && ControllerJogar.ficarSemJogar4 == false){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(true);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(false);
				} else if(ControllerJogar.ficarSemJogar1 == true && ControllerJogar.ficarSemJogar2 == true && ControllerJogar.ficarSemJogar3 == true && ControllerJogar.ficarSemJogar4 == false){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(true);
				} else if(ControllerJogar.ficarSemJogar1 == true && ControllerJogar.ficarSemJogar2 == true && ControllerJogar.ficarSemJogar3 == false && ControllerJogar.ficarSemJogar4 == true){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(true);
					jogarDado4.setEnabled(false);
				} else if(ControllerJogar.ficarSemJogar1 == true && ControllerJogar.ficarSemJogar2 == false && ControllerJogar.ficarSemJogar3 == true && ControllerJogar.ficarSemJogar4 == true){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(true);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(true);
				} else if(ControllerJogar.ficarSemJogar1 == false && ControllerJogar.ficarSemJogar2 == true && ControllerJogar.ficarSemJogar3 == true && ControllerJogar.ficarSemJogar4 == true){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(true);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(false);
				} else if(ControllerJogar.ficarSemJogar1 == true && ControllerJogar.ficarSemJogar2 == true && ControllerJogar.ficarSemJogar3 == true && ControllerJogar.ficarSemJogar4 == true){
					jogarDado0.setEnabled(true);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(false);
					ControllerJogar.ficarSemJogar1 = false;
					ControllerJogar.ficarSemJogar2 = false;
					ControllerJogar.ficarSemJogar3 = false;
					ControllerJogar.ficarSemJogar4 = false;
				} 
				timer.start();
				panelTabuleiro.removeAll();
				c.gridx = ControllerJogar.y0;
				c.gridy = ControllerJogar.x0;
				c.anchor = GridBagConstraints.FIRST_LINE_START;
				panelTabuleiro.add(ok0, c);
				c.gridx = ControllerJogar.y1;
				c.gridy = ControllerJogar.x1;
				c.anchor = GridBagConstraints.FIRST_LINE_END;
				panelTabuleiro.add(ok1, c);
				c.gridx = ControllerJogar.y2;
				c.gridy = ControllerJogar.x2;
				c.anchor = GridBagConstraints.CENTER;
				panelTabuleiro.add(ok2, c);
				c.gridx = ControllerJogar.y3;
				c.gridy = ControllerJogar.x3;
				c.anchor = GridBagConstraints.LAST_LINE_START;
				panelTabuleiro.add(ok3, c);
				c.gridx = ControllerJogar.y4;
				c.gridy = ControllerJogar.x4;
				c.anchor = GridBagConstraints.LAST_LINE_END;
				panelTabuleiro.add(ok4, c);
				
				tabuleiro(quantCasas(qntCasas));
				panelTabuleiro.revalidate();
			}
			if(e.getSource() == jogarDado1){
				
				timer.stop();
				ControllerJogar.jogar(1, corPeao[1], nomePeao[1]);
				
				if(ControllerJogar.jogarDnv1 == false){
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(true);
				} else {
					jogarDado1.setEnabled(true);
					jogarDado2.setEnabled(false);
					ControllerJogar.jogarDnv1 = false;
				}
				
				if(ControllerJogar.ficarSemJogar0 == true && ControllerJogar.ficarSemJogar2 == false && ControllerJogar.ficarSemJogar3 == false && ControllerJogar.ficarSemJogar4 == false){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(true);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(false);
				} else if(ControllerJogar.ficarSemJogar0 == false && ControllerJogar.ficarSemJogar2 == true && ControllerJogar.ficarSemJogar3 == false && ControllerJogar.ficarSemJogar4 == false){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(true);
					jogarDado4.setEnabled(false);
					ControllerJogar.ficarSemJogar2 = false;
				} else if(ControllerJogar.ficarSemJogar0 == false && ControllerJogar.ficarSemJogar2 == false && ControllerJogar.ficarSemJogar3 == true && ControllerJogar.ficarSemJogar4 == false){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(true);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(false);
				} else if(ControllerJogar.ficarSemJogar0 == false && ControllerJogar.ficarSemJogar2 == false && ControllerJogar.ficarSemJogar3 == false && ControllerJogar.ficarSemJogar4 == true){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(true);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(false);
				} else if(ControllerJogar.ficarSemJogar0 == true && ControllerJogar.ficarSemJogar2 == true && ControllerJogar.ficarSemJogar3 == false && ControllerJogar.ficarSemJogar4 == false){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(true);
					jogarDado4.setEnabled(false);
					ControllerJogar.ficarSemJogar2 = false;
				} else if(ControllerJogar.ficarSemJogar0 == false && ControllerJogar.ficarSemJogar2 == false && ControllerJogar.ficarSemJogar3 == true && ControllerJogar.ficarSemJogar4 == true){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(true);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(false);
				} else if(ControllerJogar.ficarSemJogar0 == true && ControllerJogar.ficarSemJogar2 == false && ControllerJogar.ficarSemJogar3 == true && ControllerJogar.ficarSemJogar4 == false){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(true);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(false);
				} else if(ControllerJogar.ficarSemJogar0 == false && ControllerJogar.ficarSemJogar2 == true && ControllerJogar.ficarSemJogar3 == false && ControllerJogar.ficarSemJogar4 == true){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(true);
					jogarDado4.setEnabled(false);
					ControllerJogar.ficarSemJogar2 = false;
				} else if(ControllerJogar.ficarSemJogar0 == true && ControllerJogar.ficarSemJogar2 == false && ControllerJogar.ficarSemJogar3 == false && ControllerJogar.ficarSemJogar4 == true){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(true);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(false);
				} else if(ControllerJogar.ficarSemJogar0 == false && ControllerJogar.ficarSemJogar2 == true && ControllerJogar.ficarSemJogar3 == true && ControllerJogar.ficarSemJogar4 == false){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(true);
					ControllerJogar.ficarSemJogar2 = false;
					ControllerJogar.ficarSemJogar3 = false;
				} else if(ControllerJogar.ficarSemJogar0 == true && ControllerJogar.ficarSemJogar2 == true && ControllerJogar.ficarSemJogar3 == true && ControllerJogar.ficarSemJogar4 == false){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(true);
				} else if(ControllerJogar.ficarSemJogar0 == true && ControllerJogar.ficarSemJogar2 == true && ControllerJogar.ficarSemJogar3 == false && ControllerJogar.ficarSemJogar4 == true){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(true);
					jogarDado4.setEnabled(false);
				} else if(ControllerJogar.ficarSemJogar0 == true && ControllerJogar.ficarSemJogar2 == false && ControllerJogar.ficarSemJogar3 == true && ControllerJogar.ficarSemJogar4 == true){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(true);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(true);
				} else if(ControllerJogar.ficarSemJogar0 == false && ControllerJogar.ficarSemJogar2 == true && ControllerJogar.ficarSemJogar3 == true && ControllerJogar.ficarSemJogar4 == true){
					jogarDado0.setEnabled(true);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(false);
					ControllerJogar.ficarSemJogar2 = false;
					ControllerJogar.ficarSemJogar3 = false;
					ControllerJogar.ficarSemJogar4 = false;
				} else if(ControllerJogar.ficarSemJogar0 == true && ControllerJogar.ficarSemJogar2 == true && ControllerJogar.ficarSemJogar3 == true && ControllerJogar.ficarSemJogar4 == true){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(true);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(false);
					ControllerJogar.ficarSemJogar0 = false;
					ControllerJogar.ficarSemJogar2 = false;
					ControllerJogar.ficarSemJogar3 = false;
					ControllerJogar.ficarSemJogar4 = false;
				}
				
				timer.start();
				panelTabuleiro.removeAll();
				c.gridx = ControllerJogar.y0;
				c.gridy = ControllerJogar.x0;
				c.anchor = GridBagConstraints.FIRST_LINE_START;
				panelTabuleiro.add(ok0, c);
				c.gridx = ControllerJogar.y1;
				c.gridy = ControllerJogar.x1;
				c.anchor = GridBagConstraints.FIRST_LINE_END;
				panelTabuleiro.add(ok1, c);
				c.gridx = ControllerJogar.y2;
				c.gridy = ControllerJogar.x2;
				c.anchor = GridBagConstraints.CENTER;
				panelTabuleiro.add(ok2, c);
				c.gridx = ControllerJogar.y3;
				c.gridy = ControllerJogar.x3;
				c.anchor = GridBagConstraints.LAST_LINE_START;
				panelTabuleiro.add(ok3, c);
				c.gridx = ControllerJogar.y4;
				c.gridy = ControllerJogar.x4;
				c.anchor = GridBagConstraints.LAST_LINE_END;
				panelTabuleiro.add(ok4, c);
				
				tabuleiro(quantCasas(qntCasas));
				panelTabuleiro.revalidate();
			}
			if(e.getSource() == jogarDado2){
				
				timer.stop();
				ControllerJogar.jogar(2, corPeao[2], nomePeao[2]);
				
				if(ControllerJogar.jogarDnv2 == false){
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(true);
				} else {
					jogarDado2.setEnabled(true);
					jogarDado3.setEnabled(false);
					ControllerJogar.jogarDnv2 = false;
				}
				
				if(ControllerJogar.ficarSemJogar0 == true && ControllerJogar.ficarSemJogar1 == false && ControllerJogar.ficarSemJogar3 == false && ControllerJogar.ficarSemJogar4 == false){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(true);
					jogarDado4.setEnabled(false);
				} else if(ControllerJogar.ficarSemJogar0 == false && ControllerJogar.ficarSemJogar1 == true && ControllerJogar.ficarSemJogar3 == false && ControllerJogar.ficarSemJogar4 == false){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(true);
					jogarDado4.setEnabled(false);
				} else if(ControllerJogar.ficarSemJogar0 == false && ControllerJogar.ficarSemJogar1 == false && ControllerJogar.ficarSemJogar3 == true && ControllerJogar.ficarSemJogar4 == false){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(true);
					ControllerJogar.ficarSemJogar3 = false;
				} else if(ControllerJogar.ficarSemJogar0 == false && ControllerJogar.ficarSemJogar1 == false && ControllerJogar.ficarSemJogar3 == false && ControllerJogar.ficarSemJogar4 == true){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(true);
					jogarDado4.setEnabled(false);
				} else if(ControllerJogar.ficarSemJogar0 == true && ControllerJogar.ficarSemJogar1 == true && ControllerJogar.ficarSemJogar3 == false && ControllerJogar.ficarSemJogar4 == false){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(true);
					jogarDado4.setEnabled(false);
				} else if(ControllerJogar.ficarSemJogar0 == false && ControllerJogar.ficarSemJogar1 == false && ControllerJogar.ficarSemJogar3 == true && ControllerJogar.ficarSemJogar4 == true){
					jogarDado0.setEnabled(true);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(false);
					ControllerJogar.ficarSemJogar3 = false;
					ControllerJogar.ficarSemJogar4 = false;
				} else if(ControllerJogar.ficarSemJogar0 == true && ControllerJogar.ficarSemJogar1 == false && ControllerJogar.ficarSemJogar3 == true && ControllerJogar.ficarSemJogar4 == false){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(true);
					ControllerJogar.ficarSemJogar3 = false;
				} else if(ControllerJogar.ficarSemJogar0 == false && ControllerJogar.ficarSemJogar1 == true && ControllerJogar.ficarSemJogar3 == false && ControllerJogar.ficarSemJogar4 == true){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(true);
					jogarDado4.setEnabled(false);
				} else if(ControllerJogar.ficarSemJogar0 == true && ControllerJogar.ficarSemJogar1 == false && ControllerJogar.ficarSemJogar3 == false && ControllerJogar.ficarSemJogar4 == true){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(true);
					jogarDado4.setEnabled(false);
				} else if(ControllerJogar.ficarSemJogar0 == false && ControllerJogar.ficarSemJogar1 == true && ControllerJogar.ficarSemJogar3 == true && ControllerJogar.ficarSemJogar4 == false){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(true);
					ControllerJogar.ficarSemJogar3 = false;
				} else if(ControllerJogar.ficarSemJogar0 == true && ControllerJogar.ficarSemJogar1 == true && ControllerJogar.ficarSemJogar3 == true && ControllerJogar.ficarSemJogar4 == false){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(true);
				} else if(ControllerJogar.ficarSemJogar0 == true && ControllerJogar.ficarSemJogar1 == true && ControllerJogar.ficarSemJogar3 == false && ControllerJogar.ficarSemJogar4 == true){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(true);
					jogarDado4.setEnabled(false);
				} else if(ControllerJogar.ficarSemJogar0 == true && ControllerJogar.ficarSemJogar1 == false && ControllerJogar.ficarSemJogar3 == true && ControllerJogar.ficarSemJogar4 == true){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(true);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(true);
					ControllerJogar.ficarSemJogar0 = false;
					ControllerJogar.ficarSemJogar3 = false;
					ControllerJogar.ficarSemJogar4 = false;
				} else if(ControllerJogar.ficarSemJogar0 == false && ControllerJogar.ficarSemJogar1 == true && ControllerJogar.ficarSemJogar3 == true && ControllerJogar.ficarSemJogar4 == true){
					jogarDado0.setEnabled(true);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(false);
					ControllerJogar.ficarSemJogar1 = false;
					ControllerJogar.ficarSemJogar3 = false;
					ControllerJogar.ficarSemJogar4 = false;
				} else if(ControllerJogar.ficarSemJogar0 == true && ControllerJogar.ficarSemJogar1 == true && ControllerJogar.ficarSemJogar3 == true && ControllerJogar.ficarSemJogar4 == true){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(true);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(false);
					ControllerJogar.ficarSemJogar0 = false;
					ControllerJogar.ficarSemJogar1 = false;
					ControllerJogar.ficarSemJogar3 = false;
					ControllerJogar.ficarSemJogar4 = false;
				}
				
				timer.start();
				panelTabuleiro.removeAll();
				c.gridx = ControllerJogar.y0;
				c.gridy = ControllerJogar.x0;
				c.anchor = GridBagConstraints.FIRST_LINE_START;
				panelTabuleiro.add(ok0, c);
				c.gridx = ControllerJogar.y1;
				c.gridy = ControllerJogar.x1;
				c.anchor = GridBagConstraints.FIRST_LINE_END;
				panelTabuleiro.add(ok1, c);
				c.gridx = ControllerJogar.y2;
				c.gridy = ControllerJogar.x2;
				c.anchor = GridBagConstraints.CENTER;
				panelTabuleiro.add(ok2, c);
				c.gridx = ControllerJogar.y3;
				c.gridy = ControllerJogar.x3;
				c.anchor = GridBagConstraints.LAST_LINE_START;
				panelTabuleiro.add(ok3, c);
				c.gridx = ControllerJogar.y4;
				c.gridy = ControllerJogar.x4;
				c.anchor = GridBagConstraints.LAST_LINE_END;
				panelTabuleiro.add(ok4, c);
				
				tabuleiro(quantCasas(qntCasas));
				panelTabuleiro.revalidate();
			}
			if(e.getSource() == jogarDado3){
				
				timer.stop();
				ControllerJogar.jogar(3, corPeao[3], nomePeao[3]);
				
				if(ControllerJogar.jogarDnv3 == false){
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(true);
				} else {
					jogarDado3.setEnabled(true);
					jogarDado4.setEnabled(false);
					ControllerJogar.jogarDnv3 = false;
				}
				
				if(ControllerJogar.ficarSemJogar0 == true && ControllerJogar.ficarSemJogar1 == false && ControllerJogar.ficarSemJogar2 == false && ControllerJogar.ficarSemJogar4 == false){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(true);
				} else if(ControllerJogar.ficarSemJogar0 == false && ControllerJogar.ficarSemJogar1 == true && ControllerJogar.ficarSemJogar2 == false && ControllerJogar.ficarSemJogar4 == false){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(true);
				} else if(ControllerJogar.ficarSemJogar0 == false && ControllerJogar.ficarSemJogar1 == false && ControllerJogar.ficarSemJogar2 == true && ControllerJogar.ficarSemJogar4 == false){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(true);
				} else if(ControllerJogar.ficarSemJogar0 == false && ControllerJogar.ficarSemJogar1 == false && ControllerJogar.ficarSemJogar2 == false && ControllerJogar.ficarSemJogar4 == true){
					jogarDado0.setEnabled(true);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(false);
					ControllerJogar.ficarSemJogar4 = false;
				} else if(ControllerJogar.ficarSemJogar0 == true && ControllerJogar.ficarSemJogar1 == true && ControllerJogar.ficarSemJogar2 == false && ControllerJogar.ficarSemJogar4 == false){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(true);
				} else if(ControllerJogar.ficarSemJogar0 == false && ControllerJogar.ficarSemJogar1 == false && ControllerJogar.ficarSemJogar2 == true && ControllerJogar.ficarSemJogar4 == true){
					jogarDado0.setEnabled(true);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(false);
					ControllerJogar.ficarSemJogar3 = false;
					ControllerJogar.ficarSemJogar4 = false;
				} else if(ControllerJogar.ficarSemJogar0 == true && ControllerJogar.ficarSemJogar1 == false && ControllerJogar.ficarSemJogar2 == true && ControllerJogar.ficarSemJogar4 == false){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(true);
				} else if(ControllerJogar.ficarSemJogar0 == false && ControllerJogar.ficarSemJogar1 == true && ControllerJogar.ficarSemJogar2 == false && ControllerJogar.ficarSemJogar4 == true){
					jogarDado0.setEnabled(true);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(false);
					ControllerJogar.ficarSemJogar4 = false;
				} else if(ControllerJogar.ficarSemJogar0 == true && ControllerJogar.ficarSemJogar1 == false && ControllerJogar.ficarSemJogar2 == false && ControllerJogar.ficarSemJogar4 == true){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(true);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(false);
					ControllerJogar.ficarSemJogar4 = false;
				} else if(ControllerJogar.ficarSemJogar0 == false && ControllerJogar.ficarSemJogar1 == true && ControllerJogar.ficarSemJogar2 == true && ControllerJogar.ficarSemJogar4 == false){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(true);
				} else if(ControllerJogar.ficarSemJogar0 == true && ControllerJogar.ficarSemJogar1 == true && ControllerJogar.ficarSemJogar2 == true && ControllerJogar.ficarSemJogar4 == false){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(true);
					ControllerJogar.ficarSemJogar0 = false;
					ControllerJogar.ficarSemJogar1 = false;
					ControllerJogar.ficarSemJogar2 = false;
				} else if(ControllerJogar.ficarSemJogar0 == true && ControllerJogar.ficarSemJogar1 == true && ControllerJogar.ficarSemJogar2 == false && ControllerJogar.ficarSemJogar4 == true){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(true);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(false);
					ControllerJogar.ficarSemJogar0 = false;
					ControllerJogar.ficarSemJogar2 = false;
					ControllerJogar.ficarSemJogar4 = false;
				} else if(ControllerJogar.ficarSemJogar0 == true && ControllerJogar.ficarSemJogar1 == false && ControllerJogar.ficarSemJogar2 == true && ControllerJogar.ficarSemJogar4 == true){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(true);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(false);
					ControllerJogar.ficarSemJogar0 = false;
					ControllerJogar.ficarSemJogar2 = false;
					ControllerJogar.ficarSemJogar4 = false;
				} else if(ControllerJogar.ficarSemJogar0 == false && ControllerJogar.ficarSemJogar1 == true && ControllerJogar.ficarSemJogar2 == true && ControllerJogar.ficarSemJogar4 == true){
					jogarDado0.setEnabled(true);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(false);
					ControllerJogar.ficarSemJogar1 = false;
					ControllerJogar.ficarSemJogar2 = false;
					ControllerJogar.ficarSemJogar4 = false;
				} else if(ControllerJogar.ficarSemJogar0 == true && ControllerJogar.ficarSemJogar1 == true && ControllerJogar.ficarSemJogar2 == true && ControllerJogar.ficarSemJogar4 == true){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(true);
					jogarDado4.setEnabled(false);
					ControllerJogar.ficarSemJogar0 = false;
					ControllerJogar.ficarSemJogar1 = false;
					ControllerJogar.ficarSemJogar2 = false;
					ControllerJogar.ficarSemJogar4 = false;
				}
				
				timer.start();
				panelTabuleiro.removeAll();
				c.gridx = ControllerJogar.y0;
				c.gridy = ControllerJogar.x0;
				c.anchor = GridBagConstraints.FIRST_LINE_START;
				panelTabuleiro.add(ok0, c);
				c.gridx = ControllerJogar.y1;
				c.gridy = ControllerJogar.x1;
				c.anchor = GridBagConstraints.FIRST_LINE_END;
				panelTabuleiro.add(ok1, c);
				c.gridx = ControllerJogar.y2;
				c.gridy = ControllerJogar.x2;
				c.anchor = GridBagConstraints.CENTER;
				panelTabuleiro.add(ok2, c);
				c.gridx = ControllerJogar.y3;
				c.gridy = ControllerJogar.x3;
				c.anchor = GridBagConstraints.LAST_LINE_START;
				panelTabuleiro.add(ok3, c);
				c.gridx = ControllerJogar.y4;
				c.gridy = ControllerJogar.x4;
				c.anchor = GridBagConstraints.LAST_LINE_END;
				panelTabuleiro.add(ok4, c);
				
				tabuleiro(quantCasas(qntCasas));
				panelTabuleiro.revalidate();
			}
			if(e.getSource() == jogarDado4){
				
				timer.stop();
				ControllerJogar.jogar(4, corPeao[4], nomePeao[4]);
				
				if(ControllerJogar.jogarDnv4 == false){
					jogarDado4.setEnabled(false);
					jogarDado0.setEnabled(true);
				} else {
					jogarDado4.setEnabled(true);
					jogarDado0.setEnabled(false);
					ControllerJogar.jogarDnv4 = false;
				}
				
				if(ControllerJogar.ficarSemJogar0 == true && ControllerJogar.ficarSemJogar1 == false && ControllerJogar.ficarSemJogar2 == false && ControllerJogar.ficarSemJogar3 == false){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(true);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(false);
					ControllerJogar.ficarSemJogar0 = false;
				} else if(ControllerJogar.ficarSemJogar0 == false && ControllerJogar.ficarSemJogar1 == true && ControllerJogar.ficarSemJogar2 == false && ControllerJogar.ficarSemJogar3 == false){
					jogarDado0.setEnabled(true);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(false);
				} else if(ControllerJogar.ficarSemJogar0 == false && ControllerJogar.ficarSemJogar1 == false && ControllerJogar.ficarSemJogar2 == true && ControllerJogar.ficarSemJogar3 == false){
					jogarDado0.setEnabled(true);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(false);
				} else if(ControllerJogar.ficarSemJogar0 == false && ControllerJogar.ficarSemJogar1 == false && ControllerJogar.ficarSemJogar2 == false && ControllerJogar.ficarSemJogar3 == true){
					jogarDado0.setEnabled(true);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(false);
				} else if(ControllerJogar.ficarSemJogar0 == true && ControllerJogar.ficarSemJogar1 == true && ControllerJogar.ficarSemJogar2 == false && ControllerJogar.ficarSemJogar3 == false){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(true);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(false);
					ControllerJogar.ficarSemJogar0 = false;
				} else if(ControllerJogar.ficarSemJogar0 == false && ControllerJogar.ficarSemJogar1 == false && ControllerJogar.ficarSemJogar2 == true && ControllerJogar.ficarSemJogar3 == true){
					jogarDado0.setEnabled(true);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(false);
				} else if(ControllerJogar.ficarSemJogar0 == true && ControllerJogar.ficarSemJogar1 == false && ControllerJogar.ficarSemJogar2 == true && ControllerJogar.ficarSemJogar3 == false){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(true);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(false);
					ControllerJogar.ficarSemJogar0 = false;
				} else if(ControllerJogar.ficarSemJogar0 == false && ControllerJogar.ficarSemJogar1 == true && ControllerJogar.ficarSemJogar2 == false && ControllerJogar.ficarSemJogar3 == true){
					jogarDado0.setEnabled(true);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(false);
				} else if(ControllerJogar.ficarSemJogar0 == true && ControllerJogar.ficarSemJogar1 == false && ControllerJogar.ficarSemJogar2 == false && ControllerJogar.ficarSemJogar3 == true){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(true);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(false);
					ControllerJogar.ficarSemJogar0 = false;
				} else if(ControllerJogar.ficarSemJogar0 == false && ControllerJogar.ficarSemJogar1 == true && ControllerJogar.ficarSemJogar2 == true && ControllerJogar.ficarSemJogar3 == false){
					jogarDado0.setEnabled(true);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(false);
				} else if(ControllerJogar.ficarSemJogar0 == true && ControllerJogar.ficarSemJogar1 == true && ControllerJogar.ficarSemJogar2 == true && ControllerJogar.ficarSemJogar3 == false){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(true);
					jogarDado4.setEnabled(false);
				} else if(ControllerJogar.ficarSemJogar0 == true && ControllerJogar.ficarSemJogar1 == true && ControllerJogar.ficarSemJogar2 == false && ControllerJogar.ficarSemJogar3 == true){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(true);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(false);
					ControllerJogar.ficarSemJogar0 = false;
					ControllerJogar.ficarSemJogar1 = false;
					ControllerJogar.ficarSemJogar4 = false;
				} else if(ControllerJogar.ficarSemJogar0 == true && ControllerJogar.ficarSemJogar1 == false && ControllerJogar.ficarSemJogar2 == true && ControllerJogar.ficarSemJogar3 == true){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(true);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(true);
					ControllerJogar.ficarSemJogar0 = false;
					ControllerJogar.ficarSemJogar2 = false;
					ControllerJogar.ficarSemJogar3 = false;
				} else if(ControllerJogar.ficarSemJogar0 == false && ControllerJogar.ficarSemJogar1 == true && ControllerJogar.ficarSemJogar2 == true && ControllerJogar.ficarSemJogar3 == true){
					jogarDado0.setEnabled(true);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(false);
					ControllerJogar.ficarSemJogar1 = false;
					ControllerJogar.ficarSemJogar2 = false;
					ControllerJogar.ficarSemJogar3 = false;
				} else if(ControllerJogar.ficarSemJogar0 == true && ControllerJogar.ficarSemJogar1 == true && ControllerJogar.ficarSemJogar2 == true && ControllerJogar.ficarSemJogar3 == true){
					jogarDado0.setEnabled(false);
					jogarDado1.setEnabled(false);
					jogarDado2.setEnabled(false);
					jogarDado3.setEnabled(false);
					jogarDado4.setEnabled(true);
					ControllerJogar.ficarSemJogar0 = false;
					ControllerJogar.ficarSemJogar1 = false;
					ControllerJogar.ficarSemJogar2 = false;
					ControllerJogar.ficarSemJogar3 = false;
				}
				
				timer.start();
				panelTabuleiro.removeAll();
				c.gridx = ControllerJogar.y0;
				c.gridy = ControllerJogar.x0;
				c.anchor = GridBagConstraints.FIRST_LINE_START;
				panelTabuleiro.add(ok0, c);
				c.gridx = ControllerJogar.y1;
				c.gridy = ControllerJogar.x1;
				c.anchor = GridBagConstraints.FIRST_LINE_END;
				panelTabuleiro.add(ok1, c);
				c.gridx = ControllerJogar.y2;
				c.gridy = ControllerJogar.x2;
				c.anchor = GridBagConstraints.CENTER;
				panelTabuleiro.add(ok2, c);
				c.gridx = ControllerJogar.y3;
				c.gridy = ControllerJogar.x3;
				c.anchor = GridBagConstraints.LAST_LINE_START;
				panelTabuleiro.add(ok3, c);
				c.gridx = ControllerJogar.y4;
				c.gridy = ControllerJogar.x4;
				c.anchor = GridBagConstraints.LAST_LINE_END;
				panelTabuleiro.add(ok4, c);
				
				tabuleiro(quantCasas(qntCasas));
				panelTabuleiro.revalidate();
			}
		}// 5 jogadores
	}
}