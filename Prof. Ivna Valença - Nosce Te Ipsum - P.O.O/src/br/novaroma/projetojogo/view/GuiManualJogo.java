package br.novaroma.projetojogo.view;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class GuiManualJogo extends JFrame{
	private static final long serialVersionUID = 1L;

	private JLabel instrucao1, instrucao2;
	private JTextArea textArea;
	private JScrollPane scroll;
	private JButton ok = new JButton("Voltar");
	
	public GuiManualJogo(){
		super("Sobre o Jogo");
		setLayout(new FlowLayout());

		setIconImage((new ImageIcon(getClass().getResource("/imagens/icone.png"))).getImage());
		
		setBackground(Color.BLACK);
		
		instrucao1 = new JLabel("N�o sabe como jogar? Ou como alterar algo do seu jogo?", new ImageIcon(getClass().getResource
				("/imagens/oculos.png")), 0);
		add(instrucao1);
		instrucao2 = new JLabel("Simples! Leia o manual logo abaixo.");
		add(instrucao2);
		
		
		textArea = new JTextArea(" Cadastrando um jogo: \n\t* Para cadastrar um novo jogo, v� at� o Menu, clique em <Jogo>,"
				+ "<Novo Jogo> e preencha o formul�rio com nome do jogo, casas especiais, quantidade de casas, quantidade de pe�es"
				+ "e plano de fundo."
				+ "\nObs.: O jogo n�o pode ser criado sem um nome e nem se esse nome conter mais de 15 caracteres. Se o plano de"
				+ "fundo n�o for escolhido, ser� usado o padr�o (Azul Claro)."
				
				+ "\n\n Buscando um jogo:"
				+ "\n\t* Para buscar um jogo, v� at� o Menu clique em <Jogo>, <Buscar> e digite o nome do jogo exatamente"
				+ "como foi cadastrado para visualizar todas as informa��es sobre o jogo."
				
				+ "\n\n Excluindo um jogo:"
				+ "\n\t* Para excluir um jogo, v� at� o Menu, clique em <Jogo>, <Excluir> e digite o nome exatamente como o jogo"
				+ "foi cadastrado. "
				
				+ "\n\n Alterando o nome de um jogo:"
				+ "\n\t* Para alterar o nome de um jogo, v� at� o Menu, clique em <Alterar>, <Nome do Jogo> e digite o nome"
				+ "exatamente como o jogo foi cadastrado, em seguida digite o novo nome desejado."
				
				+ "\n\n Alterando a quantidade de casas de um jogo:"
				+ "\n\t* Para alterar a quantidade de casas de um jogo, v� at� o Menu, clique em <Alterar>, <Quantidade de Casas>"
				+ "e digite o nome exatamente como o jogo foi cadastrado, em seguida, escolha a nova quantidade de casas."
				
				+ "\n\n Alterando a quantidade de pe�es de um jogo:"
				+ "\n\t* Para alterar a quantidade de pe�es de um jogo, v� at� o Menu, clique em <Alterar>, <Quantidade de Pe�es>"
				+ "e digite o nome exatamente como o jogo foi cadastrado, em seguida, escolha a nova quantidade de pe�es."
				
				+ "\n\n Alterando o plano de fundo de um jogo:"
				+ "\n\t* Para alterar o plano de fundo de um jogo j� cadastrado, v� at� o Menu e clique em <Alterar>,"
				+ "<Plano de Fundo> e digite o nome exatamente como o jogo foi cadastrado, em seguida, clique em <Procurar Novo>"
				+ "e escolha uma nova imagem (\".png\" ou \".jpg\"), caso prefira a imagem que foi cadastrada junto com o jogo clique"
				+ "em <Antiga Imagem> ou se preferir <Cor Padr�o> (Azul Claro)."
				
				+ "\n\n Iniciando um jogo:"
				+ "\n\t* Iniciar Jogo: Para iniciar o jogo, clique em <Inicirar Jogo>, logo na tela inicial e escolha um dos jogos j�"
				+ "cadastrados, escolha as cores dos pe�es e seus respectivos nomes. Clique em <Jogar> e o jogo come�ar�."
				+ "\nObs.: N�o poder� escolher a mesma cor para os pe�es e nem se um dos nomes dos pe�es conter mais de 9"
				+ "caracteres ."
				
				+ "\n\n Jogando:"
				+ "\n\t 1- Logo que iniciar o jogo voc� ter� 1 minuto para fazer sua jogada, caso acabe o tempo ser� a"
				+ " vez do pr�ximo jogador."
				+ "\n\t 2- Para o pe�o andar, clique no bot�o <Jogar Dado> dispon�vel, logo abaixo dos nomes dos jogadores, em seguida"
				+ " ser� vizualizado o n�mero que deu no dado, clique em <Ok> e pronto."
				+ "\n\t 3- O jogo ter� fim quando o primeiro pe�o chegar ou passar da casa \"Chegada\", que est� logo no final do"
				+ " tabuleiro."
				+ "\nObs.: Os bot�es que est�o ao lado do tabuleiro possui atalhos, confira em Atalhos do Sistema, logo abaixo."
				
				+ "\n\n Atalhos do Sistema:"
				+ "\n- Para abrir as op��es do menu:"
				+ "\n Jogo: Alt + 1"
				+ "\n Alterar: Alt + 2"
				+ "\n Sobre: Alt + 3"
				+ "\n Sair: Alt + 4"
				
				+ "\n\n- Para abrir as funcionalidades dos menus:"
				+ "\n Novo Jogo: Ctrl + N"
				+ "\n Buscar: Ctrl + B"
				+ "\n Excluir: Ctrl + E"
				+ "\n Alterar Nome do Jogo: Ctrl + J"
				+ "\n Alterar Quantidade de Casas: Ctrl + Q"
				+ "\n Alterar Quantidade de Pe�es: Ctrl + P"
				+ "\n Alterar Plano de Fundo: Ctrl + F"
				+ "\n Jogo: Ctrl + H"
				+ "\n Desenvolvedores: Ctrl + D"
				+ "\n Sair: Ctrl + S"
				
				+ "\n\n- Para iniciar jogo:"
				+ "\n Bot�o \"Iniciar Jogo\": Alt + J"
				
				+ "\n\n- Com o jogo j� iniciado:"
				+ "\n Bot�o \"Jogar Dado\" do 1� jogador: Alt + 1"
				+ "\n Bot�o \"Jogar Dado\" do 2� jogador: Alt + 2"
				+ "\n Bot�o \"Jogar Dado\" do 3� jogador: Alt + 3"
				+ "\n Bot�o \"Jogar Dado\" do 4� jogador: Alt + 4"
				+ "\n Bot�o \"Jogar Dado\" do 5� jogador: Alt + 5"
				
				+ "\n\n Divirta-se!", 23, 38);
		textArea.setFont(new Font("Verdana", Font.PLAIN, 13));
		textArea.setBackground(Color.LIGHT_GRAY);
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		textArea.setEditable(false);
		scroll = new JScrollPane(textArea);
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		add(scroll);

		ok.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				dispose();
			}
		});

		add(ok);

		setSize(450, 520);
		setVisible(true);
		setResizable(false);
		setLocationRelativeTo(null);
	}
}