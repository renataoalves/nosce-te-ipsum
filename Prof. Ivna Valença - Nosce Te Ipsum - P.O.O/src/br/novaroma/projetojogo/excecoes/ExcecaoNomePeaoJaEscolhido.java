package br.novaroma.projetojogo.excecoes;

import javax.swing.JOptionPane;

public class ExcecaoNomePeaoJaEscolhido extends Exception{
	private static final long serialVersionUID = 1L;

	public ExcecaoNomePeaoJaEscolhido(){
		JOptionPane.showMessageDialog(null, "Nomes iguais foram escolhidos para 2 ou mais pe�es.", "Nomes Iguais", 2);
	}
	
}
