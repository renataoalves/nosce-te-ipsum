package br.novaroma.projetojogo.excecoes;

import javax.swing.JOptionPane;

public class ExcecaoNomeInvalido extends Exception {
	private static final long serialVersionUID = 1L;

	public ExcecaoNomeInvalido(){
		JOptionPane.showMessageDialog(null, "Nome Inv�lido. Informe um v�lido.", "Cadastro de Jogo", 2);
	}
}
