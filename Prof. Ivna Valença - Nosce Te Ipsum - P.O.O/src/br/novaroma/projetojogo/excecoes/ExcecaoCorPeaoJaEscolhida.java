package br.novaroma.projetojogo.excecoes;

import javax.swing.JOptionPane;

public class ExcecaoCorPeaoJaEscolhida  extends Exception {
	private static final long serialVersionUID = 1L;

	public ExcecaoCorPeaoJaEscolhida(){
		JOptionPane.showMessageDialog(null, "Cores iguais foram escolhidas para 2 ou mais pe�es.\nPor favor, escolha cores diferentes.", "Escolhido Cores Iguais", 2);
	}
	
}
