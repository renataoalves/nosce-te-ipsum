package br.novaroma.projetojogo.model;

import br.novaroma.projetojogo.excecoes.ExcecaoNomeNaoInformado;

public class Jogo {
		
	private String nomeJogo, background, qntCasas;
	private String[] casasEspeciais;
	private int qntPeoes;
	private int [][] tabuleiro = new int [8][8];

	public Jogo(){
		background = "Azul Claro";
	}
	public Jogo (String nomeJogo, String qntCasas, int[][] tabuleiro, int peoes, String background, String[] casasEspeciais) throws ExcecaoNomeNaoInformado{
		setNomeJogo(nomeJogo);
		setQntCasas(qntCasas);
		setTabuleiro(qntCasas);
		setQntPeoes(peoes);
		setBackground(background);
		setCasasEspeciais(casasEspeciais);
	}
	public void setNomeJogo(String nome) throws ExcecaoNomeNaoInformado{
		if(!(nome.equals("")))
			this.nomeJogo = nome;
		else
			throw new ExcecaoNomeNaoInformado();
	}
	public String getNomeJogo(){
		return this.nomeJogo;
	}
	public void setQntCasas(String qntCasas){
		if(!(qntCasas.equals("")) || qntCasas != null)
			this.qntCasas = qntCasas;
	}
	public String getQntCasas(){
		return this.qntCasas;
	}
	public void setQntPeoes(int qntPeoes){
		this.qntPeoes = qntPeoes;
	}
	public int getQntPeoes(){
		return this.qntPeoes;
	}
	public void setBackground(String background){
		this.background = background;
	}
	public String getBackground(){
		return background;
	}
	public void setTabuleiro(String numCasas) {
		
		int numDeCasas = 0;
		
		if(numCasas.equals("36 casas"))
			numDeCasas = 35;
		else if(numCasas.equals("32 casas"))
			numDeCasas = 31;
		else if(numCasas.equals("28 casas"))
			numDeCasas = 27;
		
		int [][] tabuleiro = new int [8][8];
		int naoExiste = 0;
		
		if(numDeCasas == 35){
			
			for (int x = 0; x<8; x++){
				for (int y = 0; y<8; y++){
				
					if(y==0)
						tabuleiro[x][y] = 7-x;
					else if (y==2)
						tabuleiro[x][y] = 9+x;
					else if (y==4)
						tabuleiro[x][y] = 25-x;
					else if (y==6)
						tabuleiro[x][y] = 27+x;
					
					if(x==0){
						if(y==3 || y==7)
							tabuleiro[x][y] = naoExiste;
						else if (y==1)
							tabuleiro[x][y] = 8;
						else if (y==5)
							tabuleiro[x][y] = 26;
					} else if(x==1){
						if (y==1 || y==3 || y==5 || y==7)
							tabuleiro [x][y] = naoExiste;
					} else if(x==2){
						if (y==1 || y==3 || y==5 || y==7)
							tabuleiro [x][y] = naoExiste;
					} else if(x==3){
						if (y==1 || y==3 || y==5 || y==7)
							tabuleiro [x][y] = naoExiste;
					} else if(x==4){
						if (y==1 || y==3 || y==5 || y==7)
							tabuleiro [x][y] = naoExiste;
					} else if(x==5){
						if (y==1 || y==3 || y==5 || y==7)
							tabuleiro [x][y] = naoExiste;
					} else if(x==6){
						if (y==1 || y==3 || y==5 || y==7)
							tabuleiro [x][y] = naoExiste;
					} else if(x==7){
						if (y==1 || y==5)
							tabuleiro [x][y] = naoExiste;
						else if(y==3)
							tabuleiro [x][y] = 17;
						else if (y==7)
							tabuleiro[x][y] = 35;
					}
				}
			}
		}
		
		if(numDeCasas == 31){
			for (int x = 0; x<8; x++){
				for (int y = 0; y<8; y++){
					
					if(y==0)
						tabuleiro[x][y] = 7-x;
					else if (y==2)
						tabuleiro[x][y] = 7+x;
					else if (y==4)
						tabuleiro[x][y] = 23-x;
					else if (y==6)
						tabuleiro[x][y] = 23+x;
					
					if(x==0)
						tabuleiro[x][y] = naoExiste;
					else if(x==1){						
						if (y==3 || y==7)
							tabuleiro [x][y] = naoExiste;
						else if (y==1)
							tabuleiro[x][y] = 7;
						else if (y==5)
							tabuleiro[x][y] = 23;
					} else if(x==2){
						if (y==1 || y==3 || y==5 || y==7)
							tabuleiro [x][y] = naoExiste;
					} else if(x==3){
						if (y==1 || y==3 || y==5 || y==7)
							tabuleiro [x][y] = naoExiste;
					} else if(x==4){
						if (y==1 || y==3 || y==5 || y==7)
							tabuleiro [x][y] = naoExiste;
					} else if(x==5){
						if (y==1 || y==3 || y==5 || y==7)
							tabuleiro [x][y] = naoExiste;
					} else if(x==6){
						if (y==1 || y==3 || y==5 || y==7)
							tabuleiro [x][y] = naoExiste;
					} else if(x==7){
						if (y==1 || y==5)
							tabuleiro [x][y] = naoExiste;
						else if (y==3)
							tabuleiro[x][y] = 15;
						else if (y==7)
							tabuleiro[x][y] = 31;
					}
				} 
			}
		}
		
		if(numDeCasas == 27){
			for (int x = 0; x<8; x++){
				for (int y = 0; y<8; y++){
					
					if(y==0)
						tabuleiro[x][y] = 6-x;
					else if (y==2)
						tabuleiro[x][y] = 6+x;
					else if (y==4)
						tabuleiro[x][y] = 20-x;
					else if (y==6)
						tabuleiro[x][y] = 20+x;
					
					if(x==0)
						tabuleiro[x][y] = naoExiste;
					else if(x==1){
						if (y==3 || y==7)
							tabuleiro [x][y] = naoExiste;
						else if(y==1)
							tabuleiro[x][y] = 6;
						else if(y==5)
							tabuleiro [x][y] = 20;
					} else if(x==2){
						if (y==1 || y==3 || y==5 || y==7)
							tabuleiro [x][y] = naoExiste;
					} else if(x==3){
						if (y==1 || y==3 || y==5 || y==7)
							tabuleiro [x][y] = naoExiste;
					} else if(x==4){
						if (y==1 || y==3 || y==5 || y==7)
							tabuleiro [x][y] = naoExiste;
					} else if(x==5){
						if (y==1 || y==3 || y==5 || y==7)
							tabuleiro [x][y] = naoExiste;
					} else if(x==6){
						if (y==1 || y==5)
							tabuleiro [x][y] = naoExiste;
						else if(y==3)
							tabuleiro [x][y] = 13;
						else if(y==7)
							tabuleiro [x][y] = 27;
					} else if(x==7)
						tabuleiro [x][y] = naoExiste;
				} 
			}
		}
		
			this.tabuleiro = tabuleiro;
	}
	public int [][] getTabuleiro() {
		return this.tabuleiro;
	}
	public void setCasasEspeciais(String[] casasEspeciais) {
		this.casasEspeciais = casasEspeciais;
	}
	public String[] getCasasEspeciais() {
		return casasEspeciais;
	}
}